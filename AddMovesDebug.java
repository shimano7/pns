package go;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;

import javax.swing.text.html.HTMLDocument.Iterator;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class AddMovesDebug {
	static final int inf = 10000000;
	static final int or = 1; // OR Node
	static final int and = -1; // AND Node
	static final int proven = 1;
	static final int disproven = -1;
	static final int unknown = 0;
	static final int space = 0;
	static final int black = 1;
	static final int white = 2;
	static int Xrange = 0;
	static int Yrange = 0;
	static boolean koFlag = false;
	static int koMove = 0;

	static int node_cnt = 0;
	static int th_sim = 0;
	static double sim_run_cnt = 0;
	static double sim_success_cnt = 0;

	static int[][] black_move_cnt = new int[100][100];// 攻め方が着手した回数
	static int[][] attacker_success = new int[100][100];// 成功した回数
	static double[][] attacker_success_probability = new double[100][100];// 成功率

	static double R = 0.9;
	static int p_visit_limit = 10;
	static int q_visit_limit = 1;
	static int q_depth_limit = 4;

	static boolean deepPN_run = false;
	static boolean pns_run = true;

	static boolean debug = false;
	
	
	static boolean end_search(GoNode node) {
		// GoBoard.DisplayBoard(node);
		int legal_cnt = 0;
		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if (GoPlay.CheckLegal(node, node.color, x, y))
					legal_cnt++;
			}
		}

		if (legal_cnt == 0) {
			return true;
		}

		return false;
	}

	static boolean pass(GoNode node) {
		if (node.color == white) {
			if (end_search(node))
				return true;

			int space_cnt = 0;
			int black_legal_cnt = 0;
			int white_legal_cnt = 0;
			for (int y = 1; y < GoNode.Yrange + 1; y++) {
				for (int x = 1; x < GoNode.Xrange + 1; x++) {
					if (node.board[y][x] == space)
						space_cnt++;
					if (GoPlay.CheckLegal(node, black, x, y))
						black_legal_cnt++;
					if (GoPlay.CheckLegal(node, white, x, y))
						white_legal_cnt++;
				}
			}
			if (space_cnt == 2 && black_legal_cnt == 0)
				return true;
			// if(space_cnt >= 3 && white_legal_cnt == 2)
			// return true;
		}
		return false;
	}

	/* Evaluate */
	static void evaluate(GoNode node, int rootColor) {
		type(node, rootColor);
		if(debug){
			GoBoard.DisplayBoard(node);
			System.out.println("evaluate: "+GoRule.tumego_black_win(node));
		}
		// System.out.println(Yrange);
		// int eva = GoRule.CaptureStoneRule(node.agehama_cnt);
		if (GoRule.tumego_black_win(node)) {
			node.evaluate = proven; // root-win
		} else if (end_search(node) && node.color == black)
			node.evaluate = disproven; // root-loss or draw
		else {
			node.evaluate = unknown; // unknown
		}
		// System.out.println(node.evaluate);
		// GoBoard.DisplayBoard(node);
	}

	/* AND Node or OR Node */
	static void type(GoNode node, int rootColor) {
		if (node.color == rootColor)
			node.type = or;
		else if (node.color == GoBoard.ReverseColor(rootColor))
			node.type = and;
		else
			throw new RuntimeException();
	}

	/* DPNの設定 */
	static void setDPN(GoNode node) {
		if (deepPN_run) {
			if (node.type == and) {
				if (node.pn == 0 || node.dn == 0) {
					node.dpn = inf;
				} else {
					node.dpn = (1 - 1.0 / node.pn) * R + node.deep * (1.0 - R);
					if (node.dpn < 0) {
						System.out.println(node.deep);
						System.out.println("pn: " + node.pn);
						System.out.println("dn: " + node.dn);

						throw new RuntimeException("dpn");
					}
				}
			} else if (node.type == or) {
				if (node.dn == 0 || node.pn == 0) {
					node.dpn = inf;
				} else {
					node.dpn = (1 - 1.0 / node.dn) * R + node.deep * (1.0 - R);
					if (node.dpn < 0) {
						System.out.println(node.deep);
						System.out.println("pn: " + node.pn);
						System.out.println("dn: " + node.dn);

						throw new RuntimeException("dpn");
					}
				}
			}
		}
	}

	/* Calculating proof and disproof numbers */
	static void setProofNumberAndDisproofNumbers(GoNode node, String reason) {
		// GoBoard.DisplayBoard(node);
		node_cnt++;
		if (node.pns_expanded) { // Internal node

			if (node.type == and) { // AND node
				node.pn = 0;
				node.dn = inf;

				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					node.pn = node.pn + child.pn;
					if (node.pn > inf) {
						node.pn = inf;
					}
					if (child.dn < node.dn) // min
						node.dn = child.dn;
				}

				/*
				 * if(node.pn >= inf && node.dn >0){ for (int i = 0; i <
				 * node.children.size(); i++) { GoNode child =
				 * node.children.get(i); System.out.println("child_pn: "
				 * +child.pn); System.out.println("child_dn: "+child.dn);
				 * 
				 * } throw new RuntimeException(); }
				 */
				Heuristic.success_probability(node, false);
			} else { // OR Node
				node.pn = inf;
				node.dn = 0;
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					node.dn = node.dn + child.dn;
					if (node.dn > inf) {
						node.dn = inf;
					}

					if (child.pn < node.pn) // min
						node.pn = child.pn;
				}
				/*
				 * if(node.dn >= inf && node.pn >0){ throw new
				 * RuntimeException(); }
				 */
			}
			if (node.pn == 0 || node.dn == 0) {
			}
			if (deepPN_run) {
				GoNode nc = null;
				double val = inf;
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					if (child.pn != 0 && child.dn != 0) {
						if (child.dpn < val) {
							nc = child;
							val = nc.dpn;
						}
					}
				}
				if (nc != null) {
					node.deep = nc.deep;
				} else {
					node.deep = -1;
				}
				setDPN(node);
			}
		} else { // Terminal or non-Terminal leaf
			node.deep = 1 / (node.depth + 1.0);

			switch (node.evaluate) {
			case disproven:
				node.pn = inf;
				node.dn = 0;

				break;
			case proven:
				node.pn = 0;
				node.dn = inf;
				break;
			case unknown:
				node.pn = 1;
				node.dn = 1;
				Heuristic.h_legal(node, false);
				break;
			}
			setDPN(node);

		}
	}

	/* Select a MPN */
	static GoNode selectMostProvingNode(GoNode node, PrintWriter pw) {

		if (node == null)
			throw new RuntimeException();
		if (node.pn == 0) {
			throw new RuntimeException("pn = 0");

		}
		int depth = 0;
		while (true) {
			if (node == null) {
				System.out.println("null");
				throw new RuntimeException();

			}
			// node.visit_cnt = node.visit_cnt + 1;

			if (!node.pns_expanded)
				break;
			// System.out.println("smp");
			// GoBoard.DisplayBoard(node);

			double value = inf;
			GoNode best = null;
			// GoBoard.DisplayBoard(node);

			for (int i = 0; i < node.children.size(); i++) {
				GoNode child = node.children.get(i);

				// System.out.println("i: "+i);
				// System.out.println("smp_child.pn: "+child.pn);
				// System.out.println("smp_child.dn: "+child.dn);
				// System.out.println("child_dpn: "+child.dpn);
				// System.out.println("smp_child_type: "+child.type);

				// System.out.println("child.ko=" + child.ko[0] + "" +
				// child.ko[1]+","+child.ko[2] );

				if (child.ko[0] != 0 && child.ko[1] != 0)
					child.ko[2] = depth;
				if (node.parent != null && node.parent.parent != null && child.ko[0] == node.parent.parent.move_xy[0]
						&& child.ko[1] == node.parent.parent.move_xy[1] && child.ko[2] > 0 && child.ko[0] > 0
						&& child.ko[1] > 0 && node.parent.parent.move_xy[0] > 0 && node.parent.parent.move_xy[1] > 0)
					koFlag = true;
				else
					koFlag = false;
				/*
				 * if (node.parent != null && node.parent.parent != null) {
				 * 
				 * System.out.println("node.parent.move_xy= " +
				 * node.parent.parent.move_xy[0] + "" +
				 * node.parent.parent.move_xy[1]); }
				 * System.out.println(node.color);
				 */
				if (koFlag && child.color == black) {
					// System.out.println("child_type: "+child.type);
					child.pn = inf;
					child.dn = 0;
					child.dpn = inf - 1;

					node.pn = inf;
					node.dn = 0;
					node.dpn = inf - 1;

				//	node.reason = "Ko";
				//	child.reason = "Ko";

					return node;
				}

				if (deepPN_run) {
					if (value > child.dpn) {
						best = child;
						value = child.dpn;
						// System.out.println("best is "+i);
					}
				}

				if (pns_run) {
					if (node.type == or) {
						if (value > child.pn) {
							best = child;
							value = child.pn;
						}
					} else {
						if (value > child.dn) {
							best = child;
							value = child.dn;
						}
					}
				}
				// System.out.println("best: ");
				// GoBoard.DisplayBoard(best);
			}

			++depth;
			if (best == null || best.pn == 0 || best.dn == 0) {
				System.out.println("node.type: " + node.type);
				System.out.println("node.pn: " + node.pn);
				System.out.println("node.dn: " + node.dn);
				System.out.println("node.visit_cnt: " + node.visit_cnt);
				System.out.println("node.pns_expand: " + node.pns_expanded);
			//	System.out.println("node.sim_expand: " + node.simulation_expanded);

				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);

					System.out.println("child: ");
					System.out.println("child.pn: " + child.pn);
					System.out.println("child.dn: " + child.dn);
					System.out.println("child.visit_cnt: " + child.visit_cnt);
					System.out.println("child.sim_success: " + child.success_flag);
				//	System.out.println("child.reason: " + child.reason);

					GoBoard.DisplayBoard(child);
				}
				throw new RuntimeException();
			}
			// System.out.println("depth= " + depth + " move " + best.move_xy[0]
			// + best.move_xy[1]);

			node = best;
			if (pw != null) {
				pw.print(node.st_move + " " + node.pn + "," + node.dn + " " + koFlag + " ");
			}
			node.visit_cnt++;
		}
		if (pw != null) {

			pw.println("node_cnt = " + node_cnt);
		}
		return node;
	}

	/* Expand node */
	static void expandNode(GoNode node, int rootColor) {
		// System.out.println("expandNode: ");
		// GoBoard.DisplayBoard(node);
		if(debug){
			System.out.println("move: "+node.st_move);
			GoBoard.DisplayBoard(node);
		}
		
		if (pass(node)) {
			GoNode child = node.CreateChild(node, 15, 15, node.color);
			node.children.add(child);
		}

		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if (GoPlay.CheckLegal(node, node.color, x, y)) {
					GoNode child = node.CreateChild(node, x, y, node.color);
					if (node.color == black) {
						black_move_cnt[y][x]++;// 攻め方が着手した場所を記録
					}
					node.children.add(child);

				}
			}
		}

		for (int i = 0; i < node.children.size(); i++) {
			GoNode child = node.children.get(i);
			evaluate(child, rootColor);
			setProofNumberAndDisproofNumbers(child, "Expand");
			if(debug){
				System.out.println("move: "+child.st_move);
				System.out.println("child.pn: "+child.pn);
				System.out.println("child.dn: "+child.dn);
				GoBoard.DisplayBoard(child);

			}

		}
		node.pns_expanded = true;
	}

	/* Update ancestors */
	static GoNode updateAncestors(GoNode node, Deque<GoNode> proof_que, PrintWriter pw) {
		while (true) {
			setProofNumberAndDisproofNumbers(node, "updateAncestors");
			if (pw != null) {
				pw.print(" " + node.st_move + " " + node.pn + "," + node.dn);
			}

			if (node.pn == 0 && node.visit_cnt >= p_visit_limit) {
				proof_que.offer(node);
			}
			if (node.parent == null) {
				if (pw != null) {
					pw.println();
				}
				return node;
			}
			node = node.parent;
		}
	}

	static int count_space(GoNode node) {
		int cnt = 0;
		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if (node.board[y][x] == space) {
					cnt++;
				}
			}
		}
		return cnt;
	}

	static ArrayList<GoNode> searchSiblingPlus(GoNode p, boolean kishimoto) {
		ArrayList<GoNode> q_list = new ArrayList<GoNode>();
		if (p.parent != null) {
			for (int i = 0; i < p.parent.children.size(); i++) {
				GoNode child = p.parent.children.get(i);
				if (child.visit_cnt >= q_visit_limit && child != p && child.pn != 0 && child.dn != 0) {
					q_list.add(child);
				}
			}
		}
		if (!kishimoto) {
			if (p.parent == null || p.parent.parent == null) {
				return q_list;
			}
			GoNode root = p.parent.parent;
			Deque<GoNode> queue = new ArrayDeque<GoNode>();
			queue.offer(root);

			while (!queue.isEmpty()) {
				GoNode child = queue.poll();
				// GoBoard.DisplayBoard(child);
				if (p.parent != null && child.parent != null && child.id != p.id && child.parent.id != p.parent.id
						&& child.visit_cnt >= q_visit_limit && child.pn != 0 && child.dn != 0 && child.type == or
						&& SelectQ.similarPosition(p, child)) {
					q_list.add(child);
				}
				for (int i = 0; i < child.children.size(); i++) {
					if (child.depth - root.depth <= q_depth_limit && child.visit_cnt > 0 && child.pn != 0
							&& child.dn != 0) {
						queue.offer(child.children.get(i));
					}
				}
			}
		}
		return q_list;

	}

	static boolean check_terminal_children(GoNode node) {
		for (int i = 0; i < node.children.size(); i++) {
			GoNode p = node.children.get(i);
			if (p.children.size() == 0)
				return false;
		}
		return true;
	}

	static void pathMap(HashMap<Integer, String> path) {
		path.put(1, String.valueOf(32));
		path.put(2, String.valueOf(61));

		path.put(3, String.valueOf(42));
		path.put(4, String.valueOf(31));
		// path.put(4, String.valueOf(21));
		// path.put(4, String.valueOf(32));
		// path.put(4, String.valueOf(51));
		// path.put(4, String.valueOf(61));
		// path.put(5, String.valueOf(51));
		// path.put(6, String.valueOf(32));

		// path.put(5, String.valueOf(61));
		// path.put(6, String.valueOf(12));
		// path.put(7, String.valueOf(51));
		// path.put(8, String.valueOf(32));
		// path.put(9, String.valueOf(21));

	}

	/* シミュレーションに入ったときのP,Qペア(ルートのみ) */
	static void show_pair_only_root(GoNode p, GoNode q, boolean bug_check) {
		if (bug_check) {
			System.out.println("p_root_id: " + p.id);
			GoBoard.DisplayBoard(p);
			System.out.println("q_root_id: " + q.id);
			GoBoard.DisplayBoard(q);

		}
	}

	static void showPath(GoNode node, HashMap<Integer, String> path) {
		GoNode child = null;
		for (int i = 0; i < path.size(); i++) {
			for (int j = 0; j < node.children.size(); j++) {
				child = node.children.get(j);
				if (child.st_move.equals(path.get(child.depth))) {
					System.out.println("depth: " + child.depth);
					System.out.println("child.visit_cnt: " + child.visit_cnt);
					System.out.println("move: " + child.st_move);

					System.out.println("pn = " + child.pn);
					System.out.println("dn = " + child.dn);
					System.out.println("dpn = " + child.dpn);

					GoBoard.DisplayBoard(child);
					node = child;
				}
			}
		}
		// showChildren(node);
		// showProof(node);
	}

	static void showProof(GoNode node) {
		while (node.children.size() != 0) {
			GoNode child = null;
			if (node.type == or) {
				for (int i = 0; i < node.children.size(); i++) {
					child = node.children.get(i);
					if (child.pn == 0) {
						System.out.println("depth: " + child.depth);
						System.out.println("move: " + child.st_move);
						System.out.println("move: " + child.st_move);
						System.out.println("child_visit_cnt: " + child.visit_cnt);

						GoBoard.DisplayBoard(child);
						node = child;
					}
				}
			} else {
				child = node.children.get(0);
				System.out.println("depth: " + child.depth);
				System.out.println("move: " + child.st_move);
				System.out.println("child_visit_cnt: " + child.visit_cnt);
				GoBoard.DisplayBoard(child);
				node = child;
			}
		}
	}

	static void showChildren(GoNode node) {
		for (int i = 0; i < node.children.size(); i++) {
			GoNode child = node.children.get(i);
			System.out.println("pn = " + child.pn);
			System.out.println("dn = " + child.dn);
			System.out.println("dpn = " + child.dpn);

			System.out.println("depth: " + child.depth);
			System.out.println("move: " + child.st_move);
			System.out.println("move: " + child.st_move);
			System.out.println("child_visit_cnt: " + child.visit_cnt);
			GoBoard.DisplayBoard(child);
		}
	}

	/* PNS */
	static void proofNumberSearch(GoNode root, int rootColor, boolean simulation, int k) throws IOException {
		node_cnt = 0;
		int th = 50000;
		int search_node_limit = 50000000;
		HashMap<Integer, Integer> map = new HashMap<Integer, Integer>();
		File file = null;
		PrintWriter pw = null;

		if (k == 3) {
			file = new File("/Users/shimanotakuya/Desktop/node_cnt.txt");
			pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));
		}

		GoRule.RootCoordinates(root);
		root.color = rootColor;

		evaluate(root, rootColor);
		setProofNumberAndDisproofNumbers(root, "Root");
		Deque<GoNode> proof_que = new ArrayDeque<GoNode>();

		GoNode current = root;

		int loop_cnt = 1;
		int start_line = 368;
		int end_line = 368;

		while (root.pn != 0 && root.dn != 0) {
			debug = false;
			GoNode mpn = null;
			if (loop_cnt >= start_line && loop_cnt <= end_line) {
				debug = true;
				mpn = selectMostProvingNode(current, pw);
			} else {
				mpn = selectMostProvingNode(current, null);
			}
			if (node_cnt > search_node_limit) {
				System.out.println("path");
				while (mpn != null) {
					System.out.println("node");
					GoBoard.DisplayBoard(mpn);
					mpn = mpn.parent;
				}
				break;
			}
			/*
			 * System.out.println("mpn.type: " + mpn.type);
			 * 
			 * System.out.println("mpn.depth: " + mpn.depth);
			 * System.out.println("mpn.deep: " + mpn.deep);
			 * 
			 * System.out.println("mpn.dpn: " + mpn.dpn); System.out.println(
			 * "mpn.pn: " + mpn.pn); System.out.println("mpn.dn: " + mpn.dn);
			 * 
			 * if (mpn.pn == 0 || mpn.dn == 0) { throw new RuntimeException(
			 * "mpn error");
			 * 
			 * }
			 */
			if (mpn.pn != 0 || mpn.dn != 0) {
				expandNode(mpn, rootColor);
			}

			if (loop_cnt >= start_line && loop_cnt <= end_line) {
				current = updateAncestors(mpn, proof_que, pw);
			} else {
				current = updateAncestors(mpn, proof_que, null);
			}
			loop_cnt++;
			// System.out.println("current.deep: "+current.deep);
			// System.out.println("current.dpn: "+current.dpn);

			// System.out.println("sim_run_cnt: " + sim_run_cnt);

			// System.out.println("node_cnt: " + node_cnt);
			if (simulation) {
				boolean kishimoto = false;

				if (sim_run_cnt < th && !proof_que.isEmpty()) {
					GoNode p = proof_que.poll();// searchProvenNode(current);
					if (p != null && check_terminal_children(p)) {
						// System.out.println("p.visit: " + p.visit_cnt);
						/*
						 * int count = 1; if(map.containsKey(p.visit_cnt)){
						 * count += map.get(p.visit_cnt); } map.put(p.visit_cnt,
						 * count);
						 */
						// System.out.println("p");
						// GoBoard.DisplayBoard(p);
						ArrayList<GoNode> q_list = new ArrayList<GoNode>();
						q_list = searchSiblingPlus(p, kishimoto);

						if (!q_list.isEmpty()) {
							for (int i = 0; i < q_list.size(); i++) {
								GoNode q = q_list.get(i);
								// show_pair_only_root(p, q, false);
								if (sim_run_cnt < th) {
									Simulation2.Simulation(p, q, black);
									sim_run_cnt += 1;
								}

								if (q.success_flag) {
									sim_success_cnt += 1;
									show_pair_only_root(p, q, false);

								}
							}
						}
					}
				}
			}
		}
		if(pw != null){
			pw.close();
		}
		/*
		 * java.util.Iterator<Integer> iterator = map.keySet().iterator();
		 * while(iterator.hasNext()) { Integer key = iterator.next(); Integer
		 * value = map.get(key); System.out.println(key + ": " + value); }
		 */

		// System.out.println("sim_run_cnt: " + sim_run_cnt);
		// System.out.println("sim_success_cnt: " + sim_success_cnt);

		// System.out.println("node: " + node_cnt);

		// System.out.println("root-pn: " + root.pn);
		// System.out.println("root-dn: " + root.dn);

		// for (int i = 1; i < p_depth_array.length; i++) {
		// if (p_depth_array[i] != 0)
		// System.out.println(i + ": " + p_depth_array[i]);
		// }
		HashMap<Integer, String> path = new HashMap<Integer, String>();
		pathMap(path);
		// 9showPath(root, path);
		// showChildren(root);

		if (root.pn == 0) {
			// showProof(root);
		}
		if (root.dn == 0) {
			// showDisproof(root);
		}
		// System.out.println("node_cnt: " + node_cnt);
		// node_cnt = 0;
	}
}