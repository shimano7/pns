package go;

public class GoBoard {
	static final int space = 0;
	static final int black = 1;
	static final int white = 2;
	static final int out = 3;

	/* initilaize the board */
	static void InitializeBoard(GoNode node) {
		for (int y = 1; y < node.size() - 1; y++) {
			for (int x = 1; x < node.size() - 1; x++) {
				node.board[y][x] = space;
			}
		}
		for (int y = 0; y < node.size(); y++) {
			node.board[y][0] = out;
			node.board[y][node.size() - 1] = out;
			node.board[0][y] = out;
			node.board[node.size() - 1][y] = out;
		}
	}

	/* initialize the checkboard */
	static void ClearCheckBoard(boolean[][] checkBoard, int size) {
		for (int y = 1; y < size - 1; y++) {
			for (int x = 1; x < size - 1; x++) {
				checkBoard[y][x] = false;
			}
		}
	}

	/* copy of the board */
	static int[][] CopyBoard(int[][] board) {
		int[][] boardClone = new int[board.length][];
		for (int i = 0; i < board.length; i++) {
			boardClone[i] = board[i].clone();
		}
		return boardClone;
	}

	/* reverse the stone color */
	static int ReverseColor(int stoneColor) {
		return black + white - stoneColor;
	}

	/* show the board */
	static void DisplayBoard(GoNode node){
		for (int y = 0; y < node.size(); y++) {
			if (y <= node.size() - 2)
				System.out.print(" ");
			for (int x = 0; x < node.size(); x++) {
				if (node.board[y][x] == space)
					System.out.print("+");
				else if (node.board[y][x] == black)
					System.out.print("*");
				else if (node.board[y][x] == white)
					System.out.print("o");
				//else
					//System.out.print("?");
				if (x != node.size() - 1)
					System.out.print(" ");
			}
			System.out.println();
		}
	}
}