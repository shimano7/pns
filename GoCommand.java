package go;

import java.util.Scanner;

public class GoCommand {
	static final int black = 1;
	static final int white = 2;

	static void Commands(GoNode node, int[] xy) throws Exception {
		Scanner in = new Scanner(System.in);
		String command = in.next();
		BoardSize(node, command, in);
		ShowBoard(node, command);
		InitializeBoard(node, command);
		Play(node, command, in);
		Match(node, command, in);
		Exit(command);
	}

	static void BoardSize(GoNode node, String command, Scanner in) {
		if (command.equals("boardsize")) {
			int size = in.nextInt() + 2;
			node.board = new int[size][size];
			GoBoard.InitializeBoard(node);
			node.depth = 0;
		}
	}

	static void ShowBoard(GoNode node, String command) throws Exception {
		if (command.equals("showboard"))
			GoBoard.DisplayBoard(node);
	}

	static void InitializeBoard(GoNode node, String command) {
		if (command.equals("initialize")) {
			GoBoard.InitializeBoard(node);
			node.depth = 0;
		}
	}

	static void Undo(GoNode node,String command){
		if(command.equals("undo")){
			System.out.println(node.parent);
		}
	}
	
	static void Exit(String command) {
		if (command.equals("exit"))
			System.exit(0);
	}

	static void Play(GoNode node, String command, Scanner in) {
		if (command.equals("play")) {
			command = in.next();
			if (command.equals("B") || command.equals("b")) {
				GoPlayer.HumanPlayer(node, in, black);
			} else if (command.equals("W") || command.equals("w")) {
				GoPlayer.HumanPlayer(node, in, white);
			}
		}
	}

	static void Match(GoNode node, String command, Scanner in) {
		if (command.equals("vs")) {
			// first move
			String first_move = in.next();
			if (first_move.equals("hum")) {
				// second move
				String second_move = in.next();
				if (second_move.equals("hum")) {
					GoPlayer.HumanPlayer(node, in, black);
					GoPlayer.HumanPlayer(node, in, white);
				} else if (second_move.equals("com")) {
					System.out.println("com");
				}
			} else if (first_move.equals("com")) {
				String second_move = in.next();
				if (second_move.equals("hum")) {

				} else if (second_move.equals("com")) {
					System.out.println("com");
				}

			}
		}
	}
}
