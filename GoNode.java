package go;

import java.util.ArrayList;

public class GoNode {
	int size() {
		return board.length;
	}

	int[][] board;
	int[] ko = { 0, 0, 0 };
	int[] agehama_cnt = { 0, 0 };
	int depth = 0;

	static int object_id = 1;
	static final int space = 0;
	static final int black = 1;
	static final int white = 2;
	static final int or = 1;
	static final int and = -1;
	static final int proven = 1;
	static final int disproven = -1;

	ArrayList<GoNode> children = new ArrayList<GoNode>();

	double pn = 1;
	double dn = 1;
	int color = 0; // 石の色
	int type = 0; // ORノードかANDノード
	boolean pns_expanded = false; // pnsで展開されたときtrueとする
	int visit_cnt = 0;
	double deep = 0;
	double dpn = 0;

	int evaluate = -2;
	GoNode parent = null; // 任意のnodeの親
	int id;
	static int Xrange = 0; // 探索範囲
	static int Yrange = 0;
	int zob = 0;

	boolean koFl = false; // コウ判定に使うフラグ
	boolean success_flag = false;
	
	int[] move_xy = { 0, 0 };
	String st_move = "";
	double failure = 0;

	GoNode(int[][] initBoard) {
		this.board = GoBoard.CopyBoard(initBoard);
		this.id = object_id;
		object_id++;
	}

	public String toString() {
		String s = "";
		String st_parent = "";
		String st_type = "";
		for (int y = 1; y < size() - 1; y++) {
			for (int x = 1; x < size() - 1; x++) {
				if (board[y][x] == space)
					s += "+";
				if (board[y][x] == black)
					s += "*";
				else if (board[y][x] == white)
					s += "o";
			}
			s += "\n";
		}
		if (parent != null)
			st_parent += parent.id;
		else if (parent == null)
			st_parent += null;

		if (type == or)
			st_type += "OR";
		else if (type == and)
			st_type += "AND";
		else {
			throw new RuntimeException();
		}
		return "id: " + id + "\n" + "parent: " + st_parent + "\n" + "color: " + color + "\n" + "type: " + st_type + "\n"
				+ "pn: " + pn + "\n" + "dn: " + dn + "\n" + "黒のアゲハマ: " + agehama_cnt[0] + "\n" + "白のアゲハマ: "
				+ agehama_cnt[1] + "\n" + "expanded: " + pns_expanded + "\n" + "evaluate: " + evaluate + "\n"
				+ "move: {" + move_xy[0] + "," + move_xy[1] + "}" + "\n" + "st_move; " + st_move + "\n" + "局面: " + "\n"
				+ s + "\n" + children.toString() + " \n";

	}

	public GoNode CreateChild(GoNode node, int x, int y, int newStoneColor) {
		int[] xy = { x, y };
		GoNode new_child = new GoNode(node.board);
		GoPlay.SetStone(new_child, xy, newStoneColor);
		new_child.depth = node.depth + 1;
		new_child.color = GoBoard.ReverseColor(newStoneColor);
		new_child.parent = node;
		new_child.move_xy = xy;
		new_child.st_move = String.valueOf(xy[0]) + String.valueOf(xy[1]);
		return new_child;
	}
}
