package go;

public class GoPlay {
	static final int black = 1;
	static final int white = 2;
	static final int space = 0;

	/* check the legal moves */
	public static boolean CheckLegal(GoNode node, int stoneColor, int x, int y) {
		/* 空じゃないと置けない */
		if (node.board[y][x] != space)
			return false;

		/* 一手前にコウを取られていたら置けない */
		if (CheckKo(node, x, y))
			return false;

		/* 自殺手なら置けない */
		if (CheckSuicide(node, stoneColor, x, y) == true)
			return false;

		return true;
	}

	public static boolean CheckKo(GoNode node, int x, int y) {
		if (node.depth > 1 && node.ko[0] == x && node.ko[1] == y && node.ko[2] == (node.depth - 1))
			return true;
		return false;
	}

	/* 自殺手かどうか調べる */
	public static boolean CheckSuicide(GoNode node, int stoneColor, int x, int y) {
		boolean[][] checkBoard = new boolean[node.size()][node.size()];
		node.board[y][x] = stoneColor;
		GoBoard.ClearCheckBoard(checkBoard, node.size());
		/* 相手に石が囲まれているなら自殺手の可能性あり */
		if ((CheckRemove(node, checkBoard, stoneColor, x, y) == true)) {
			int opp = GoBoard.ReverseColor(stoneColor);// 相手の色を求める
			if (x > 1) {
				/* 隣は相手か */
				if (node.board[y][x - 1] == opp) {
					GoBoard.ClearCheckBoard(checkBoard, node.size());
					/* 相手の石は囲まれているか */
					if ((CheckRemove(node, checkBoard, opp, x - 1, y) == true)) {
						node.board[y][x] = space;
						return false;
					}
				}
			}
			if (y > 1) {
				if (node.board[y - 1][x] == opp) {
					GoBoard.ClearCheckBoard(checkBoard, node.size());
					/* 相手の石は囲まれているか */
					if ((CheckRemove(node, checkBoard, opp, x, y - 1) == true)) {
						node.board[y][x] = space;
						return false;
					}
				}
			}
			if (x < node.size() - 2) {
				if (node.board[y][x + 1] == opp) {
					GoBoard.ClearCheckBoard(checkBoard, node.size());
					/* 相手の石は囲まれているか */
					if ((CheckRemove(node, checkBoard, opp, x + 1, y) == true)) {
						node.board[y][x] = space;
						return false;
					}
				}
			}
			if (y < node.size() - 2) {
				if (node.board[y + 1][x] == opp) {
					GoBoard.ClearCheckBoard(checkBoard, node.size());
					/* 相手の石は囲まれているか */
					if ((CheckRemove(node, checkBoard, opp, x, y + 1) == true)) {
						node.board[y][x] = space;
						return false;
					}
				}
			}
			/* 盤を元に戻す */
			node.board[y][x] = space;

			/* 相手の石を取れないなら自殺手 */
			return true;
		}
		/* 盤を元に戻す */
		node.board[y][x] = space;
		/* 囲まれていないので自殺手ではない */
		return false;
	}

	/* 座標(x,y)の石が囲まれているか調べ、 囲まれていないならfalseを返す */
	public static boolean CheckRemove(GoNode node, boolean[][] checkBoard, int stoneColor, int x, int y) {
		/* 座標(x,y)が既に調べた点ならTrueを返す。まだ調べていないなら調べたことを覚えておく */
		if (checkBoard[y][x] == true) {
			return true;
		}
		/* 調べたところをTrue */
		checkBoard[y][x] = true;

		/* 座標(x,y)が空ならfalse */
		if (node.board[y][x] == space) {
			return false;
		}

		/* 座標(x,y)が同じ色ならばその石の四方を調べる。 */
		if (node.board[y][x] == stoneColor) {
			/* 左の石を調べる */
			if (x > 1) {
				if (CheckRemove(node, checkBoard, stoneColor, x - 1, y) == false) {
					return false;
				}
			}
			/* 上の石を調べる */
			if (y > 1) {
				if (CheckRemove(node, checkBoard, stoneColor, x, y - 1) == false) {
					return false;
				}
			}
			/* 右の石を調べる */
			if (x < node.size() - 2) {
				if (CheckRemove(node, checkBoard, stoneColor, x + 1, y) == false) {
					return false;
				}
			}

			/* 右下の石を調べる */
			if (y < node.size() - 2) {
				if (CheckRemove(node, checkBoard, stoneColor, x, y + 1) == false) {
					return false;
				}
			}
		}
		return true;
	}

	/* 座標(x,y)の石が死んでいれば盤から取り除く */
	public static int RemoveStone(GoNode node, int stoneColor, int x, int y) {
		boolean[][] checkBoard = new boolean[node.size()][node.size()];
		if (node.board[y][x] == stoneColor)
			return 0;

		if (node.board[y][x] == space)
			return 0;

		GoBoard.ClearCheckBoard(checkBoard, node.size());

		if (CheckRemove(node, checkBoard, node.board[y][x], x, y) == true) {
			int agehama = DoCheckRemoveStone(node, node.board[y][x], x, y, 0);
			return agehama;
		}

		return 0;
	}

	/* 座標(x,y)から石を取り除き取った石の数を返す */
	public static int DoCheckRemoveStone(GoNode node, int stoneColor, int x, int y, int agehama) {
		/* 取り除かれる石と同じ色ならば石を取る */
		if (node.board[y][x] == stoneColor) {
			/* アゲハマを1つ増やす */
			agehama++;

			/* 座標(x,y)を空にする */
			node.board[y][x] = space;

			/* 左を調べる */
			if (x > 1) {
				agehama = DoCheckRemoveStone(node, stoneColor, x - 1, y, agehama);
			}

			/* 上を調べる */
			if (y > 1) {
				agehama = DoCheckRemoveStone(node, stoneColor, x, y - 1, agehama);
			}

			/* 右を調べる */
			if (x < node.size() - 2) {
				agehama = DoCheckRemoveStone(node, stoneColor, x + 1, y, agehama);
			}

			/* 下を調べる */
			if (y < node.size() - 2) {
				agehama = DoCheckRemoveStone(node, stoneColor, x, y + 1, agehama);
			}
		}
		/* 取った石を返す */
		return agehama;
	}

	/* 置いた石の周囲の石が死んでいれば盤から取り除く */
	public static int[] RemoveSurroundStone(GoNode node, int stoneColor, int x, int y) {
		int[] agehama = { 0, 0, 0, 0, 0 };// 上、右、下、左、合計
		if (y > 1) {
			agehama[0] = RemoveStone(node, stoneColor, x, y - 1);
		}
		if (x > 1) {
			agehama[3] = RemoveStone(node, stoneColor, x - 1, y);
		}
		if (y < node.size() - 2) {
			agehama[2] = RemoveStone(node, stoneColor, x, y + 1);
		}
		if (x < node.size() - 2) {
			agehama[1] = RemoveStone(node, stoneColor, x + 1, y);
		}
		/* 取られた石の数 */
		agehama[4] = agehama[0] + agehama[1] + agehama[2] + agehama[3];
		return agehama;
	}

	/* コウの時の座標 */
	public static int[] KoGridChart(GoNode node, int[] agehama, int x, int y) {
		int[] ko = { 0, 0, 0 };// x,y,手数
		/* 置いた石のとなりに同じ色の石がなく、取り除かれた石も1つならコウ */
		if (node.koFl == true && agehama[4] == 1) {
			/* コウの発生した手数を覚える */
			ko[2] = node.depth;
			/* コウの座標を覚える */
			if (agehama[1] == 1) {
				/* 取り除かれた石が右 */
				ko[0] = x + 1;
				ko[1] = y;
			} else if (agehama[2] == 1) {
				/* 取り除かれた石が下 */
				ko[0] = x;
				ko[1] = y + 1;
			} else if (agehama[3] == 1) {
				/* 取り除かれた石が左 */
				ko[0] = x - 1;
				ko[1] = y;
			} else if (agehama[0] == 1) {
				/* 取り除かれた石が上 */
				ko[0] = x;
				ko[1] = y - 1;
			}
		}
		return ko;
	}

	/* 石を打つ */
	public static void SetStone(GoNode node, int[] xy, int stoneColor) {
		// boolean koFlag = false;
		if (CheckLegal(node, stoneColor, xy[0], xy[1]) == true) {
			node.board[xy[1]][xy[0]] = stoneColor;

			/* 置いた隣の石の隣に同じ色の石はあるか？ */
			if (node.board[xy[1] + 1][xy[0]] != stoneColor && node.board[xy[1] - 1][xy[0]] != stoneColor
					&& node.board[xy[1]][xy[0] + 1] != stoneColor && node.board[xy[1]][xy[0] - 1] != stoneColor) {
				/* 同じ色の石がないならコウの可能性あり */
				node.koFl = true;
			} else {
				/* 同じ色の石がないならコウでない */
				node.koFl = false;
			}

			/* 置いた石の周囲の石が死んでいれば盤から取り除く */
			int[] agehama = RemoveSurroundStone(node, stoneColor, xy[0], xy[1]);

			node.ko = KoGridChart(node, agehama, xy[0], xy[1]);

			/* アゲハマの更新 */
			if (agehama[4] > 0) {
				if (stoneColor == black)
					node.agehama_cnt[0] += agehama[4];
				else if (stoneColor == white)
					node.agehama_cnt[1] += agehama[4];
			}
		}
	}

	/* メイン */
	public static void main(String[] args) throws Exception {
		int max_size = 19;
		int width = max_size + 2;
		int[][] board = new int[width][width];
		int[] xy = { 0, 0 };

		GoNode node = new GoNode(board);
		while (true) {
			GoCommand.Commands(node, xy);
		}
	}
}
