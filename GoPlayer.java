package go;

import java.util.Random;
import java.util.Scanner;

public class GoPlayer {
	/* Human player */
	static void HumanPlayer(GoNode node, Scanner in, int stoneColor) {
		int[] xy = new int[2];
		xy[0] = in.nextInt();
		xy[1] = in.nextInt();
		if ((xy[0] < 1 || node.size() - 2 < xy[0])
				|| (xy[1] < 1 || node.size() - 2 < xy[1])
				|| GoPlay.CheckLegal(node, stoneColor, xy[0], xy[1]) == false) {
			System.out.println("No legal move: please enter the coordinates");
			HumanPlayer(node, in, stoneColor);
		} else {
			GoPlay.SetStone(node, xy, stoneColor);
			node.depth++;
			System.out.println("move count: " + node.depth);
		}
	}

	/* Random player */
	static void RandomPlayer(GoNode node, int[] xy, int stoneColor) {
		int[][] boardClone = GoBoard.CopyBoard(node.board);
		for (int i = 0; i < 2; i++) {
			Random rnd = new Random();
			int ran = rnd.nextInt(node.size() - 2) + 1;
			xy[i] = ran;
		}
		if (GoPlay.CheckLegal(node, stoneColor, xy[0], xy[1]) == false) {
			node.board = boardClone;
			RandomPlayer(node, xy, stoneColor);
		}
	}
}
