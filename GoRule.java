package go;

public class GoRule {
	static final int black = 1;
	static final int white = 2;
	static int[] white_coordinates = new int[1000];
	static int root_white_cnt = 0;

	static boolean cs_black_win(GoNode node) {
		if (CaptureStoneRule(node.agehama_cnt) == black)
			return true;
		return false;
	}

	/* ルール1(先に石を取った方の勝ち) */
	static int CaptureStoneRule(int[] agehamaCount) {
		if (agehamaCount[0] > 0)
			return black;
		else if (agehamaCount[1] > 0)
			return white;
		return -2;
	}

	/* 詰碁 */
	static void RootCoordinates(GoNode root) {
		root_white_cnt = 0;
		for (int y = 1; y < root.size() - 1; y++) {
			for (int x = 1; x < root.size() - 1; x++) {
				if (root.board[y][x] == white) {
					white_coordinates[root_white_cnt] = XY_Trans(x, y);
					root_white_cnt++;
				}
			}

		}
	}

	static int XY_Trans(int x, int y) {
		return 100 * x + y;
	}

	static int[] RevXY(int trans_xy) {
		int[] rev_xy = { 0, 0 };
		if (trans_xy > 0) {
			rev_xy[0] = trans_xy / 100;
			rev_xy[1] = trans_xy % 100;
		}
		return rev_xy;

	}

	static boolean tumego_black_win(GoNode node) {
		int local_cnt = 0;
		for (int i = 0; i < root_white_cnt; i++) {
			int[] revXY = RevXY(white_coordinates[i]);
			if (node.board[revXY[1]][revXY[0]] == white)
				local_cnt++;
		}

		if (local_cnt <= 2)
			return true;
		return false;

	}
}
