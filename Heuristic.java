package go;

public class Heuristic {
	static final int and = -1; // AND Node
	static final int or = 1; // AND Node

	static double failure_probability(int x, int y) {
		double ans = 1 - WideMore.attacker_success_probability[y][x];
		final double min_prob = 0.0001;
		if (ans == 0)
			return min_prob;
		return ans;
	}

	static void success_probability(GoNode node, boolean run) {
		if (run) {
			if (node.pn == 0) {
				node.dn = WideMore.inf;
				WideMore.attacker_success[node.move_xy[1]][node.move_xy[0]]++;// 成功した回数を記録
				// 成功率の計算
				WideMore.attacker_success_probability[node.move_xy[1]][node.move_xy[0]] = (double) WideMore.attacker_success[node.move_xy[1]][node.move_xy[0]]
						/ (double) WideMore.black_move_cnt[node.move_xy[1]][node.move_xy[0]];

			}

			node.failure = failure_probability(node.move_xy[1], node.move_xy[0]);
			node.pn *= node.failure;
		}
	}

	static void h_legal(GoNode node, boolean run) {
		if (run) {
			if (node.type == and) {
				for (int y = 1; y < GoNode.Yrange + 1; y++) {
					for (int x = 1; x < GoNode.Xrange + 1; x++) {
						if (GoPlay.CheckLegal(node, node.color, x, y)) {
							node.pn = node.pn + 1;
						}
					}
				}
			}
			if (node.type == or) {
				for (int y = 1; y < GoNode.Yrange + 1; y++) {
					for (int x = 1; x < GoNode.Xrange + 1; x++) {
						if (GoPlay.CheckLegal(node, node.color, x, y)) {
							node.dn = node.dn + 1;
						}
					}
				}
			}
		}
	}
}
