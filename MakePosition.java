package go;

import java.util.ArrayList;
import java.util.HashMap;

public class MakePosition {
	static final int black = 1;
	static final int white = 2;
	static final int size = 19;

	static void makePosition(GoNode node, int[] blackArray, int[] whiteArray) {
		HashMap<Integer, ArrayList<Integer>> map = setPositionList(blackArray, whiteArray);
		for (int i = 0; i < map.get(black).size(); i++) {
			int[] xy = encordeXY(map.get(black).get(i));
			node.board[xy[0]][xy[1]] = black;
		}

		for (int i = 0; i < map.get(white).size(); i++) {
			int[] xy = encordeXY(map.get(white).get(i));
			node.board[xy[0]][xy[1]] = white;
		}
	}

	static HashMap<Integer, ArrayList<Integer>> setPositionList(int[] blackArray, int[] whiteArray) {
		HashMap<Integer, ArrayList<Integer>> map = new HashMap<Integer, ArrayList<Integer>>();

		// black
		ArrayList<Integer> blackList = new ArrayList<Integer>();
		for (int i = 0; i < blackArray.length; i++) {
			blackList.add(blackArray[i]);
		}
		map.put(black, blackList);

		// white
		ArrayList<Integer> whiteList = new ArrayList<Integer>();
		for (int i = 0; i < whiteArray.length; i++) {
			whiteList.add(whiteArray[i]);
		}
		map.put(white, whiteList);

		return map;
	}

	static int[] encordeXY(int xy) {
		int[] newXY = { 0, 0 };
		if (xy < 100) {
			newXY[0] = xy / 10;
			newXY[1] = xy % 10;
		}
		return newXY;
	}
}
