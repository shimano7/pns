package go;

import java.io.IOException;

public class MySimulationTest {
	static final int black = 1;
	static final int white = 2;
	static final int size = 19;

	static void Test(GoNode node, int i) {
		if (i == 1) {
			Test1(node);
		}
		if (i == 2) {
			Test2(node);
		}
		if (i == 3) {
			Test3(node);
		}
		if (i == 4) {
			Test4(node);
		}
		if (i == 5) {
			Test5(node);
		}
		if (i == 6) {
			Test6(node);
		}
		if (i == 7) {
			Test7(node);
		}
		if (i == 8) {
			Test8(node);
		}
		if (i == 9) {
			Test9(node);
		}
		if (i == 10) {
			Test10(node);
		}
		if (i == 11) {
			Test11(node);
		}
		if (i == 12) {
			Test12(node);
		}
		if (i == 13) {
			Test13(node);
		}
		if (i == 14) {
			Test14(node);
		}
		if (i == 15) {
			Test15(node);
		}
		if (i == 16) {
			Test16(node);
		}
		if (i == 17) {
			Test17(node);
		}
		if (i == 18) {
			Test18(node);
		}
		if (i == 19) {
			Test19(node);
		}
		if (i == 20) {
			Test20(node);
		}
		if (i == 21) {
			Test21(node);
		}
		if (i == 22) {
			Test22(node);
		}
		if (i == 23) {
			Test23(node);
		}
		if (i == 24) {
			Test24(node);
		}
		if (i == 25) {
			Test25(node);
		}
		if (i == 26) {
			Test26(node);
		}
		if (i == 27) {
			Test27(node);
		}
		if (i == 28) {
			Test28(node);
		}
		if (i == 29) {
			Test29(node);
		}
		if (i == 30) {
			Test30(node);
		}
		if (i == 31) {
			Test31(node);
		}
		if (i == 32) {
			Test32(node);
		}
		if (i == 33) {
			Test33(node);
		}
		if (i == 34) {
			Test34(node);
		}
		if (i == 35) {
			Test35(node);
		}
		if (i == 36) {
			Test36(node);
		}
		if (i == 37) {
			Test37(node);
		}
		if (i == 38) {
			Test38(node);
		}
		if (i == 39) {
			Test39(node);
		}
		if (i == 40) {
			Test40(node);
		}
	}

	static void Test1(GoNode node) {
		/* black */
		node.board[1][3] = black;
		node.board[1][8] = black;

		node.board[2][1] = black;
		// node.board[2][4] = black;
		node.board[2][6] = black;
		node.board[2][8] = black;

		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][7] = black;

		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		node.board[4][8] = black;

		node.board[5][3] = black;

		/* white */
		node.board[1][2] = white;
		node.board[1][5] = white;
		node.board[1][6] = white;
		node.board[1][7] = white;

		node.board[2][2] = white;
		node.board[2][3] = white;
		// node.board[2][5] = white;
		node.board[2][7] = white;

		node.board[3][4] = white;
		node.board[3][5] = white;
		node.board[3][6] = white;

		GoNode.Xrange = 7;
		GoNode.Yrange = 3;
	}

	static void Test2(GoNode node) {
		/* black */
		// node.board[1][3] = black;
		node.board[1][8] = black;

		// node.board[2][1] = black;
		// node.board[2][4] = black;
		node.board[2][6] = black;
		node.board[2][8] = black;

		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][7] = black;

		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		node.board[4][8] = black;

		node.board[5][3] = black;

		/* white */
		// node.board[1][2] = white;
		node.board[1][5] = white;
		node.board[1][6] = white;
		node.board[1][7] = white;

		// node.board[2][2] = white;
		node.board[2][3] = white;
		// node.board[2][5] = white;
		node.board[2][7] = white;

		node.board[3][4] = white;
		node.board[3][5] = white;
		node.board[3][6] = white;

		GoNode.Xrange = 7;
		GoNode.Yrange = 3;
	}

	/* p25 */
	static void Test3(GoNode node) {
		// black
		int[] setBlack = { 21, 31, 42, 62, 53, 54, 55, 45, 35, 36, 37, 27, 17, 41, 51, 46, 65, 47, 52, 55 };
		// white
		int[] setWhite = { 12, 14, 22, 25, 26, 32, 34, 43, 44 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;

	}

	static void Test4(GoNode node) {

		// black
		int[] setBlack = { 21, 31, 42, 62, 53, 54, 55, 45, 35, 36, 37, 27, 17, 41, 51, 46, 65, 47, 52 };
		// white
		int[] setWhite = { 14, 22, 25, 26, 32, 34, 43, 44 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	/* p26 */
	static void Test5(GoNode node) {
		// black
		int[] setBlack = { 21, 31, 23, 41, 42, 43, 44, 45, 36, 37, 28, 18, 46, 47, 48, 38 };
		// white
		int[] setWhite = { 12, 22, 32, 33, 35, 24, 26, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 3;

	}

	static void Test6(GoNode node) {

		// black
		int[] setBlack = { 21, 31, 23, 41, 42, 43, 44, 45, 36, 37, 28, 18, 46, 47, 48, 38 };
		// white
		int[] setWhite = { 12, 22, 33, 35, 24, 26, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 3;
	}

	/* p34 */
	static void Test7(GoNode node) {
		// black
		int[] setBlack = { 41, 42, 61, 62, 63, 53, 54, 55, 56, 57, 45, 46, 47, 17, 27, 37, 26, 36, 35, 33, 25 };
		// white
		int[] setWhite = { 12, 23, 24, 43, 44, 32, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	static void Test8(GoNode node) {
		// black
		int[] setBlack = { 41, 42, 61, 62, 63, 53, 54, 55, 56, 57, 45, 46, 47, 17, 27, 37, 26, 36, 35, 33, 25 };
		// white
		int[] setWhite = { 12, 23, 24, 43, 44, 32 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	static void Test9(GoNode node) {
		// black
		int[] setBlack = { 31, 32, 42, 43, 44, 45, 27, 38, 57, 47, 56, 17, 41, 46, 37 };
		// white
		int[] setWhite = { 26, 36, 34, 33, 22, 21, 11 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 3;
	}

	static void Test10(GoNode node) {
		// black
		int[] setBlack = { 31, 32, 42, 43, 44, 45, 27, 38, 57, 47, 56, 17, 41, 46, 37 };
		// white
		int[] setWhite = { 26, 36, 34, 33, 22, 21 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 3;
	}

	static void Test11(GoNode node) {
		// black
		int[] setBlack = { 17, 27, 35, 36, 37, 41, 42, 43, 44, 45, 46, 31 };
		// white
		int[] setWhite = { 13, 14, 22, 25, 26, 33, 34, };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 3;
	}

	static void Test12(GoNode node) {
		// black
		int[] setBlack = { 17, 27, 35, 36, 37, 41, 42, 43, 44, 45, 46 };
		// white
		int[] setWhite = { 13, 14, 22, 25, 26, 33, 34 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 3;
	}

	static void Test13(GoNode node) {
		int[] setBlack = { 17, 27, 35, 36, 37, 41, 42, 43, 44, 45, 46 };
		// white
		int[] setWhite = { 12, 25, 26, 32, 33, 34 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 3;
	}

	static void Test14(GoNode node) {
		int[] setBlack = { 17, 28, 38, 35, 36, 37, 41, 42, 43, 44, 45, 46, 18 };
		// white
		int[] setWhite = { 12, 25, 26, 32, 33, 34, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 3;
	}

	/* p33 */
	static void Test15(GoNode node) {
		int[] setBlack = { 21, 31, 32, 43, 44, 45, 46, 47, 48, 38, 28, 18, 17 };
		// white
		int[] setWhite = { 12, 22, 33, 34, 35, 36, 37, 27, 16, };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 3;
	}

	static void Test16(GoNode node) {
		int[] setBlack = { 21, 31, 32, 43, 44, 45, 46, 47, 48, 38, 17, 29, 39, 49, 18, 19 };
		// white
		int[] setWhite = { 12, 22, 33, 34, 35, 36, 37, 27, 16, 28 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 8;
		GoNode.Yrange = 3;
	}

	/* p55 */
	static void Test17(GoNode node) {
		int[] setBlack = { 21, 31, 32, 43, 44, 45, 46, 47, 48, 38, 28, 18, 41, 42, 12 };
		// white
		int[] setWhite = { 17, 27, 37, 36, 25, 33, 23, 22 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 3;
	}

	static void Test18(GoNode node) {
		int[] setBlack = { 12, 21, 31, 32, 43, 44, 45, 46, 47, 48, 38, 29, 19, 39, 18, 41, 42 };
		// white
		int[] setWhite = { 17, 27, 37, 36, 25, 33, 23, 22, 28 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 8;
		GoNode.Yrange = 3;
	}

	/* p171 */
	static void Test19(GoNode node) {
		int[] setBlack = { 31, 41, 42, 52, 53, 54, 55, 46, 36, 26, 16, 12, 45, 51 };
		// white
		int[] setWhite = { 21, 22, 32, 14, 34, 44, 25, 35 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 5;
		GoNode.Yrange = 4;
	}

	static void Test20(GoNode node) {
		int[] setBlack = { 31, 41, 42, 52, 53, 54, 55, 46, 36, 27, 16, 12, 45, 51, 17, 27, 37, 47 };
		// white
		int[] setWhite = { 21, 22, 32, 34, 44, 25, 35, 26 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	/* p183 */
	static void Test21(GoNode node) {
		int[] setBlack = { 31, 32, 33, 43, 44, 45, 46, 47, 37, 27, 17, 21, 12, 24, 42 };
		// white
		int[] setWhite = { 11, 21, 22, 23, 33, 34, 35, 36, 26 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	static void Test22(GoNode node) {
		int[] setBlack = { 31, 32, 33, 43, 44, 45, 46, 47, 37, 27, 17, 12, 24, 42 };
		// white
		int[] setWhite = { 22, 23, 33, 34, 35, 36, 26 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	/* p77 */
	static void Test23(GoNode node) {
		int[] setBlack = { 21, 31, 32, 33, 43, 44, 45, 46, 47, 48, 38, 28, 18 };
		// white
		int[] setWhite = { 22, 23, 14, 34, 35, 36, 37, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 3;
	}

	static void Test24(GoNode node) {
		int[] setBlack = { 21, 31, 32, 33, 43, 44, 45, 46, 47, 48, 38, 28, 18 };
		// white
		int[] setWhite = { 23, 14, 34, 35, 36, 37, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 3;
	}

	/* p83 */
	static void Test25(GoNode node) {
		int[] setBlack = { 41, 51, 52, 53, 43, 33, 54, 55, 56, 46, 36, 27, 17, 37, 12 };
		// white
		int[] setWhite = { 21, 31, 32, 42, 13, 23, 25, 35, 45, 16 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	static void Test26(GoNode node) {
		int[] setBlack = { 41, 52, 53, 43, 33, 54, 55, 56, 46, 36, 27, 17, 37, 12 };
		// white
		int[] setWhite = { 21, 32, 42, 13, 23, 25, 35, 45, 16 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	/* p57 */
	static void Test27(GoNode node) {
		int[] setBlack = { 41, 51, 52, 53, 54, 55, 56, 46, 36, 26, 16, 15, 24 };
		// white
		int[] setWhite = { 31, 22, 42, 43, 34, 14, 35, 25 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 5;
		GoNode.Yrange = 4;
	}

	static void Test28(GoNode node) {
		int[] setBlack = { 41, 51, 52, 53, 54, 55, 56, 46, 36, 26, 16, 15, 24 };
		// white
		int[] setWhite = { 31, 22, 42, 43, 14, 35, 25 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 5;
		GoNode.Yrange = 4;

	}

	/* p59 */
	static void Test29(GoNode node) {
		int[] setBlack = { 11, 21, 31, 32, 42, 52, 53, 54, 55, 56, 46, 47, 37, 27, 17, 41, 51, 35 };
		// white
		int[] setWhite = { 22, 13, 33, 44, 45, 36, 26, 25 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	static void Test30(GoNode node) {
		int[] setBlack = { 11, 21, 31, 32, 42, 52, 53, 54, 55, 56, 46, 47, 37, 27, 17, 41, 51, 35 };
		// white
		int[] setWhite = { 22, 13, 33, 45, 36, 26, 25 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	static void Test31(GoNode node) {
		int[] setBlack = { 21, 31, 41, 42, 43, 44, 45, 46, 47, 48, 38, 39, 29, 19, 35, 26, 32 };
		// white
		int[] setWhite = { 33, 34, 23, 25, 36, 37, 27, 17, 28 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 8;
		GoNode.Yrange = 3;
	}

	static void Test32(GoNode node) {
		int[] setBlack = { 21, 31, 41, 42, 43, 44, 45, 46, 47, 48, 38, 39, 29, 19, 35, 26 };
		// white
		int[] setWhite = { 32, 33, 34, 23, 25, 36, 37, 27, 17, 28 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 8;
		GoNode.Yrange = 3;
	}

	static void Test33(GoNode node) {
		int[] setBlack = { 11, 21, 31, 41, 32, 42, 43, 44, 54, 55, 56, 57, 47, 37, 27, 17 };
		// white
		int[] setWhite = { 12, 22, 23, 34, 45, 15, 26, 36 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	static void Test34(GoNode node) {
		int[] setBlack = { 11, 21, 31, 41, 32, 42, 43, 44, 54, 55, 56, 57, 47, 37, 27, 17 };
		// white
		int[] setWhite = { 23, 34, 45, 15, 26, 36 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	/* p91 */
	static void Test35(GoNode node) {
		int[] setBlack = { 41, 42, 32, 43, 44, 54, 55, 56, 57, 58, 48, 38, 28, 18, 17, 27, 16, 37 };
		// white
		int[] setWhite = { 21, 31, 22, 33, 34, 45, 26, 15, 36 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 4;
	}

	static void Test36(GoNode node) {
		int[] setBlack = { 41, 42, 32, 43, 44, 54, 55, 56, 57, 58, 48, 38, 28, 18, 17, 27, 16 };
		// white
		int[] setWhite = { 21, 31, 22, 33, 34, 45, 36, 26, 37, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 4;
	}

	/* 107 */
	static void Test37(GoNode node) {
		int[] setBlack = { 41, 42, 52, 53, 54, 55, 45, 46, 36, 37, 27, 17, 16, 15, 33 };
		// white
		int[] setWhite = { 22, 32, 43, 44, 34, 14, 25, 35 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 4;
	}

	static void Test38(GoNode node) {
		int[] setBlack = { 41, 42, 52, 53, 54, 55, 45, 46, 36, 37, 28, 17, 16, 15, 33, 47 };
		// white
		int[] setWhite = { 22, 32, 43, 44, 34, 14, 25, 35 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 7;
		GoNode.Yrange = 4;
	}

	static void Test39(GoNode node) {
		int[] setBlack = { 31, 41, 42, 43, 44, 45, 46, 36, 26, 27, 17, 14 };
		// white
		int[] setWhite = { 21, 22, 32, 34, 35, 25, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 3;
	}

	static void Test40(GoNode node) {
		int[] setBlack = { 31, 41, 42, 43, 44, 45, 46, 36, 27, 17, 14 };
		// white
		int[] setWhite = { 21, 22, 32, 34, 35, 25, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 3;
	}

	static void Test41(GoNode node) {
		int[] setBlack = { 31, 41, 42, 43, 44, 45, 46, 36, 27, 17, 14 };
		// white
		int[] setWhite = { 21, 22, 32, 34, 35, 25, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		GoNode.Xrange = 6;
		GoNode.Yrange = 3;
	}

	public static void main(String[] args) throws IOException {
		int n = 2;
		boolean simulation = false;
		boolean kishimoto = false;

		for (int i = 1; i <= 40; i++) {
			int[][] board = new int[size + 2][size + 2];

			GoNode p = new GoNode(board);
			GoBoard.InitializeBoard(p);

			Test(p, i);

			// GoBoard.DisplayBoard(p);
			long start = System.nanoTime();
			WideMore.proofNumberSearch(p, black, simulation, kishimoto);

			long end = System.nanoTime();

			System.out.print(WideMore.node_cnt);
			// System.out.print((end - start) / 1000000f);

			// System.out.println("Time:" + (end - start) / 1000000f + "ms");
			// System.out.print(AddMoves.node_cnt + "," + (end - start) /
			// 1000000f );
			// if(simulation)
			// System.out.printf(","+"%.2f",AddMoves.sim_success_cnt /
			// AddMoves.sim_run_cnt * 100);

			System.out.println();
		}

	}
}
