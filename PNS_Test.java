package go;

public class PNS_Test {
	static final int black = 1;
	static final int white = 2;
	static final int size = 19;

	static void TestCase1(GoNode node) {
		node.board[2][3] = black;
		node.board[3][3] = white;
	}

	static void TestCase2(GoNode node) {
		node.board[1][1] = black;
		node.board[1][2] = black;
		node.board[1][3] = black;

		node.board[2][1] = black;
		node.board[2][2] = black;
		node.board[2][3] = black;

		node.board[3][3] = white;
	}

	/* 3*3 */
	static void TestCase3(GoNode node) {
		node.board[2][3] = black;
		node.board[3][2] = white;
		node.board[3][3] = white;
	}

	/* 4*4 */
	static void TestCase4_1(GoNode node) {
		node.board[3][4] = black;
		node.board[4][3] = white;
		node.board[4][4] = white;
	}

	static void TestCase4_2(GoNode node) {
		node.board[3][2] = black;
		node.board[2][3] = black;
		node.board[3][3] = white;
	}

	static void TestCase4_3(GoNode node) {
		node.board[1][3] = white;
		node.board[2][2] = black;
	}

	static void TestCase4_4(GoNode node) {
		node.board[1][3] = white;
		node.board[2][2] = black;
		node.board[3][3] = black;
	}

	static void TestCase4_5(GoNode node) {
		node.board[1][2] = black;
		node.board[2][1] = black;
		node.board[3][2] = white;
		node.board[3][3] = black;
	}

	static void TestCase4_6(GoNode node) {
		node.board[2][2] = black;
		node.board[2][3] = black;
		node.board[3][2] = black;
		node.board[3][3] = white;
		node.board[4][3] = white;
	}

	static void TestCase4_7(GoNode node) {
		node.board[1][2] = white;
		node.board[3][2] = white;
		node.board[2][1] = black;
		node.board[2][3] = black;
	}

	static void TestCase4_8(GoNode node) {
		node.board[1][2] = white;
		node.board[2][3] = black;
		node.board[4][4] = white;
	}

	static void TestCase4_9(GoNode node) {
		node.board[2][2] = black;
		node.board[2][3] = black;
		node.board[2][4] = black;
		node.board[3][4] = black;
		node.board[3][2] = white;
		node.board[3][3] = white;
		node.board[4][2] = white;
	}

	static void TestCase4_10(GoNode node) {
		node.board[1][2] = black;
		node.board[2][2] = black;
		node.board[3][3] = black;
		node.board[2][3] = white;
		node.board[3][4] = white;
	}

	static void TestCase4_11(GoNode node) {
		node.board[2][2] = black;
		node.board[3][2] = black;
		node.board[3][3] = white;
	}
	
	
	static void TumegoTest1(GoNode node) {
		node.board[1][5] = black;
		node.board[2][5] = black;
		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][4] = black;
		node.board[3][5] = black;
		
		node.board[1][4] = white;
		node.board[2][1] = white;
		node.board[2][2] = white;
		node.board[2][3] = white;
		node.board[2][4] = white;



		node.Xrange = 5;
		node.Yrange = 3;
	}
	
	static void TumegoTestP1(GoNode node){
		/*black*/
		node.board[1][3] = black;
		node.board[1][8] = black;

		node.board[2][1] = black;
		node.board[2][4] = black;
//		node.board[2][6] = black;
		node.board[2][8] = black;	
		
		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][7] = black;
		
		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		node.board[4][8] = black;

		node.board[5][3] = black;
		
		/*white*/
		node.board[1][2] = white;
		node.board[1][5] = white;
		node.board[1][6] = white;
		node.board[1][7] = white;
		
		
		node.board[2][2] = white;
		node.board[2][3] = white;
		node.board[2][5] = white;
		node.board[2][7] = white;

		node.board[3][4] = white;
		node.board[3][5] = white;
		node.board[3][6] = white;
		

		node.Xrange = 7;
		node.Yrange = 3;
	}
	
	static void TumegoTestQ1(GoNode node){
		/*black*/
		node.board[1][3] = black;
		node.board[1][8] = black;

//		node.board[2][1] = black;
//		node.board[2][4] = black;
		node.board[2][6] = black;
		node.board[2][8] = black;	
		
		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][7] = black;
		
		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		node.board[4][8] = black;

		node.board[5][3] = black;
		
		/*white*/
		node.board[1][2] = white;
		node.board[1][5] = white;
		node.board[1][6] = white;
		node.board[1][7] = white;
		
		
	//	node.board[2][2] = white;
		node.board[2][3] = white;
	//	node.board[2][5] = white;
		node.board[2][7] = white;

		node.board[3][4] = white;
		node.board[3][5] = white;
		node.board[3][6] = white;
		

		node.Xrange = 7;
		node.Yrange = 3;
	}
	
	static void Test1(GoNode node){
		node.board[1][2]=black;
		node.board[2][1]=black;
		node.board[4][1]=black;
		node.board[4][2]=black;
		node.board[4][3]=black;
		node.board[4][4]=black;
		node.board[4][5]=black;
		node.board[1][5]=black;
		node.board[2][5]=black;
		node.board[3][5]=black;
		node.board[4][5]=black;
		
		
		node.board[2][2]=white;
		node.board[3][1]=white;
		node.board[3][2]=white;
		node.board[3][3]=white;
		node.board[1][3]=white;
		node.board[1][4]=white;
		node.board[2][4]=white;
		node.board[3][2]=white;
		node.board[3][4]=white;
		
		node.Xrange=4;
		node.Yrange=4;
		}
	
	static void Test2(GoNode node){
		node.board[1][2]=black;
		node.board[2][1]=black;
		node.board[4][1]=black;
		node.board[4][2]=black;
		node.board[4][3]=black;
		node.board[4][4]=black;
		node.board[4][5]=black;
		node.board[4][6]=black;

		node.board[1][6]=black;
		node.board[2][6]=black;
		node.board[3][6]=black;
		node.board[4][6]=black;
		node.board[5][6]=black;
			
		node.board[1][3]=white;
		node.board[1][5]=white;
		
		node.board[2][3]=white;
		node.board[2][4]=white;
		node.board[2][5]=white;

		node.board[3][1]=white;
		node.board[3][2]=white;
		node.board[3][3]=white;
		node.board[3][4]=white;
		node.board[3][5]=white;


		
		node.Xrange=4;
		node.Yrange=4;
		}
	
	static void Test3(GoNode node){
		/*black*/
		node.board[1][1] = white;

		node.board[1][3] = black;
		node.board[1][8] = black;

		node.board[2][1] = black;
		node.board[2][4] = black;
//		node.board[2][6] = black;
		node.board[2][8] = black;	
		
		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][7] = black;
		
		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		node.board[4][8] = black;

		node.board[5][3] = black;
		
		/*white*/
		//node.board[1][2] = white;
		node.board[1][5] = white;
		node.board[1][6] = white;
		node.board[1][7] = white;
		
		
		//node.board[2][2] = white;
		//node.board[2][3] = white;
		node.board[2][5] = white;
		node.board[2][7] = white;

		node.board[3][4] = white;
		node.board[3][5] = white;
		node.board[3][6] = white;
		

		node.Xrange = 7;
		node.Yrange = 3;
	}
	
	static void Test4(GoNode node){
		/*black*/
		node.board[1][3] = black;
		node.board[1][8] = black;

//		node.board[2][1] = black;
		node.board[2][4] = black;
		node.board[2][6] = black;
		node.board[2][8] = black;	
		
		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][7] = black;
		
		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		node.board[4][8] = black;

		node.board[5][3] = black;
		
		/*white*/
		node.board[1][1] = white;

		node.board[1][2] = white;
		node.board[1][5] = white;
		node.board[1][6] = white;
		node.board[1][7] = white;
		
		
	//	node.board[2][2] = white;
		node.board[2][3] = white;
	//	node.board[2][5] = white;
		node.board[2][7] = white;

		node.board[3][4] = white;
		node.board[3][5] = white;
		node.board[3][6] = white;
		

		node.Xrange = 7;
		node.Yrange = 3;
	}
	
	static void TumegoTest2(GoNode node){
		node.board[1][7] = black;
		node.board[2][7] = black;
		node.board[3][5] = black;
		node.board[3][6] = black;
		node.board[3][7] = black;
		node.board[4][1] = black;
		node.board[4][2] = black;
		node.board[4][3] = black;
		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;

		
		node.board[1][3] = white;
		node.board[1][4] = white;
		node.board[2][2] = white;
		node.board[2][5] = white;
		node.board[2][6] = white;
		node.board[3][3] = white;
		node.board[3][4] = white;
		
		node.Xrange = 6;
		node.Yrange = 3;
	}
	
	static void TumegoTest3(GoNode node){
		node.board[1][7] = black;
		node.board[2][7] = black;
		node.board[3][5] = black;
		node.board[3][6] = black;
		node.board[3][7] = black;
		node.board[4][1] = black;
		node.board[4][2] = black;
		node.board[4][3] = black;
		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		
		node.board[1][2] = white;
		node.board[2][5] = white;
		node.board[2][6] = white;
		node.board[3][2] = white;
		node.board[3][3] = white;
		node.board[3][4] = white;
		
		node.Xrange = 6;
		node.Yrange = 3;
	}
	
	static void TestProofNumberSearch1() {
		int[][] board = new int[size + 2][size + 2];
		GoNode node = new GoNode(board);
		GoBoard.InitializeBoard(node);
		
	//	TumegoTestP1(node);
		
	//	Test4(node);
		TumegoTest2(node);
	//	TumegoTestP1(node);
	//	Test3(node);
//		System.out.println("P");
		//node.color = black;
		GoNode node2 = new GoNode(board);
	//	System.out.println("Q");
		GoBoard.InitializeBoard(node2);
		
		TumegoTestQ1(node2);
//		GoBoard.DisplayBoard(node2);
		
		GoNode node3 = new GoNode(board);
		GoBoard.InitializeBoard(node3);
		TumegoTest3(node3);
		
		GoBoard.DisplayBoard(node3);

	//	GoRule.RootCoordinates(node);
		
	//	TumegoTest1(node);
		//TestCase2(node);
	//	System.out.println(node.color);

	//	ProofNumberSearch.proofNumberSearch(node3, black);

	//	GoBoard.DisplayBoard(node);
	//	GoRule.RootCoordinates(node);
	//	System.out.println(GoPlay.CheckLegal(node, black, 1, 1));

	}

	static void TestProofNumberSearch2() {
		int[][] board = new int[size + 2][size + 2];
		GoNode node = new GoNode(board);
		GoBoard.InitializeBoard(node);
		// GoBoard.DisplayBoard(node);

		TestCase4_3(node);
		
		// TestCase2(node);
		 ProofNumberSearch.proofNumberSearch(node, black);
	//	ProofNumberSearch2.proofNumberSearch(node, black);

	}

	public static void main(String[] args) {
		//TestProofNumberSearch2();
		TestProofNumberSearch1();

	}
}
