package go;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;

public class ProofNumberSearch {
	static final int inf = 1000000;
	static final int or = 1; // OR Node
	static final int and = -1; // AND Node
	static final int proven = 1;
	static final int disproven = -1;
	static final int unknown = 0;
	static final int space = 0;
	static final int black = 1;
	static final int white = 2;
	//static int Xrange = 0;
	//static int Yrange = 0;
	static boolean koFlag = false;
	static int koMove = 0;

	static int node_cnt = 0;

	static void XYrange(GoNode root) {
	//	Xrange = root.Xrange;
	//	Yrange = root.Yrange;
	}

	static boolean end_search(GoNode node) {
		// GoBoard.DisplayBoard(node);
		int legal_cnt = 0;
		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if (GoPlay.CheckLegal(node, node.color, x, y))
					legal_cnt++;
			}
		}

		if (legal_cnt == 0) {
			return true;
		}

		return false;
	}

	static boolean pass(GoNode node) {
		if (node.color == white) {
			if (end_search(node))
				return true;

			int space_cnt = 0;
			int black_legal_cnt = 0;
			int white_legal_cnt = 0;
			for (int y = 1; y < GoNode.Yrange + 1; y++) {
				for (int x = 1; x < GoNode.Xrange + 1; x++) {
					if (node.board[y][x] == space)
						space_cnt++;
					if (GoPlay.CheckLegal(node, black, x, y))
						black_legal_cnt++;
					if (GoPlay.CheckLegal(node, white, x, y))
						white_legal_cnt++;
				}
			}
			if (space_cnt == 2 && black_legal_cnt == 0)
				return true;
			// if(space_cnt >= 3 && white_legal_cnt == 2)
			// return true;
		}
		return false;
	}

	/* Evaluate */
	static void evaluate(GoNode node, int rootColor) {
		type(node, rootColor);
		// System.out.println(Yrange);
		// int eva = GoRule.CaptureStoneRule(node.agehama_cnt);
		if (GoRule.tumego_black_win(node)) {
			node.evaluate = proven; // root-win
		} else if (end_search(node) && node.color == black)
			node.evaluate = disproven; // root-loss or draw
		else {
			node.evaluate = unknown; // unknown
		}
		// System.out.println(node.evaluate);
		// GoBoard.DisplayBoard(node);
	}

	/* AND Node or OR Node */
	static void type(GoNode node, int rootColor) {
		if (node.color == rootColor)
			node.type = or;
		else if (node.color == GoBoard.ReverseColor(rootColor))
			node.type = and;
		else
			throw new RuntimeException();
	}

	/* Calculating proof and disproof numbers */
	static void setProofNumberAndDisproofNumbers(GoNode node) {
		// GoBoard.DisplayBoard(node);
		node_cnt++;
		if (node.pns_expanded) { // Internal node
			if (node.type == and) { // AND node
				node.pn = 0;
				node.dn = inf;
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					node.pn = node.pn + child.pn;
					if (child.dn < node.dn) // min
						node.dn = child.dn;
				}
			} else { // OR Node
				node.pn = inf;
				node.dn = 0;
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					node.dn = node.dn + child.dn;
					if (child.pn < node.pn) // min
						node.pn = child.pn;
				}
			}
		} else { // Terminal or non-Terminal leaf
			switch (node.evaluate) {
			case disproven:
				node.pn = inf;
				node.dn = 0;
				break;
			case proven:
				node.pn = 0;
				node.dn = inf;
				break;
			case unknown:
				node.pn = 1;
				node.dn = 1;
				break;
			}
		}
	}

	/* Select a MPN */
	static GoNode selectMostProvingNode(GoNode node) {
		int depth = 0;
		while (true) {
			if (node == null) {
				// System.out.println("null");
				// System.out.println("type: "+pre.type);
				// GoBoard.DisplayBoard(pre);
			}

			if (!node.pns_expanded)
				break;
			// System.out.println("smp");
			// GoBoard.DisplayBoard(node);

			double value = inf;
			GoNode best = null;

			for (int i = 0; i < node.children.size(); i++) {
				GoNode child = node.children.get(i);
				// System.out.println("i: "+i);
				// System.out.println("smp_child.pn: "+child.pn);
				// System.out.println("smp_child.dn: "+child.dn);
				// System.out.println("smp_child_type: "+child.type);

				// GoBoard.DisplayBoard(child);

				// System.out.println("child.ko=" + child.ko[0] + "" +
				// child.ko[1]+","+child.ko[2] );

				if (child.ko[0] != 0 && child.ko[1] != 0)
					child.ko[2] = depth;
				if (node.parent != null && node.parent.parent != null && child.ko[0] == node.parent.parent.move_xy[0]
						&& child.ko[1] == node.parent.parent.move_xy[1] && child.ko[2] > 0 && child.ko[0] > 0
						&& child.ko[1] > 0 && node.parent.parent.move_xy[0] > 0 && node.parent.parent.move_xy[1] > 0)
					koFlag = true;
				else
					koFlag = false;
				/*
				 * if (node.parent != null && node.parent.parent != null) {
				 * 
				 * System.out.println("node.parent.move_xy= " +
				 * node.parent.parent.move_xy[0] + "" +
				 * node.parent.parent.move_xy[1]); }
				 * System.out.println(node.color);
				 */
				if (koFlag && child.color == black)
					child.pn = inf;

				if (node.type == or) {
					if (value > child.pn) {
						best = child;
						value = child.pn;
					}
				} else {
					if (value > child.dn) {
						best = child;
						value = child.dn;
					}
				}
				// System.out.println("best: ");
				// GoBoard.DisplayBoard(best);
			}

			++depth;
			// if(best != null)
			// System.out.println("depth= " + depth + " move " + best.move_xy[0]
			// + best.move_xy[1]);

			node = best;
		}
		return node;
	}

	/* Expand node */
	static void expandNode(GoNode node, int rootColor) {
		// System.out.println("expandNode: ");
		// GoBoard.DisplayBoard(node);
		if (pass(node)) {
			GoNode child = node.CreateChild(node, 15, 15, node.color);
			node.children.add(child);
		}

		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if (GoPlay.CheckLegal(node, node.color, x, y)) {
					GoNode child = node.CreateChild(node, x, y, node.color);
					node.children.add(child);

				}
			}
		}

		for (int i = 0; i < node.children.size(); i++) {
			GoNode child = node.children.get(i);
			evaluate(child, rootColor);
			setProofNumberAndDisproofNumbers(child);

		}
		node.pns_expanded = true;
	}

	/* Update ancestors */
	static GoNode updateAncestors(GoNode node) {
		while (true) {
			setProofNumberAndDisproofNumbers(node);
			if (node.parent == null)
				return node;
			node = node.parent;
		}
	}
	static void showProof(GoNode node) {
		while (node.children.size() != 0) {
			GoNode child = null;
			if (node.type == or) {
				for (int i = 0; i < node.children.size(); i++) {
					child = node.children.get(i);
					if (child.pn == 0) {
						GoBoard.DisplayBoard(child);
						node = child;
					}
				}
			} else {
				child = node.children.get(0);
				GoBoard.DisplayBoard(child);
				node = child;
			}
		}
	}

	static void showProof2(GoNode node) {
		Deque<GoNode> list = new ArrayDeque<GoNode>();
		list.push(node);
		while (!list.isEmpty()) {
			if (node.type == or) {
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					if (child.pn == 0) {
						list.add(child);
					}
				}
			} else {
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					list.add(child);
				}
			}
			// System.out.println(list.size());
			node = list.poll();
			System.out.println("depth: " + node.depth);
			System.out.println("st_move: " + node.move_xy[0] + "" + node.move_xy[1]);
			GoBoard.DisplayBoard(node);
		}
	}

	static void showDisproof(GoNode node) {
		while (node.children.size() != 0) {
			GoNode child = null;
			if (node.type == and) {
				for (int i = 0; i < node.children.size(); i++) {
					child = node.children.get(i);
					if (child.dn == 0) {
						GoBoard.DisplayBoard(child);
						node = child;
					}
				}
			} else {
				child = node.children.get(0);
				GoBoard.DisplayBoard(child);
				node = child;
			}
		}
	}

	static void pathMap(HashMap<Integer, String> path) {
		path.put(1, String.valueOf(12));
		path.put(2, String.valueOf(13));
		path.put(3, String.valueOf(22));
		path.put(4, String.valueOf(32));

	}

	static void showPath(GoNode node, HashMap<Integer, String> path) {
		GoNode child = null;
		for (int i = 0; i < path.size(); i++) {
			for (int j = 0; j < node.children.size(); j++) {
				child = node.children.get(j);
				if (child.st_move.equals(path.get(child.depth))) {
					System.out.println("depth: " + child.depth);
					System.out.println("pn = " + child.pn);
					System.out.println("dn = " + child.dn);
					GoBoard.DisplayBoard(child);
					node = child;
				}
			}
		}
		showProof(node);
	}

	/* PNS */
	static void proofNumberSearch(GoNode root, int rootColor) {
		GoRule.RootCoordinates(root);
		root.color = rootColor;
		evaluate(root, rootColor);
		setProofNumberAndDisproofNumbers(root);

		GoNode current = root;
		// GoBoard.DisplayBoard(current);
		while (root.pn != 0 && root.dn != 0) {
			// if(node_cnt > 20)
			// break;
			GoNode mpn = selectMostProvingNode(current);
			// System.out.println("mpn");
			// GoBoard.DisplayBoard(mpn);
			// System.out.println("mpn.color_before: "+mpn.color);
			// System.out.println(pass(mpn));
			// if(pass(mpn))
			// mpn.color = black;
			// System.out.println("mpn.color_after: "+mpn.color);

			expandNode(mpn, rootColor);
			current = updateAncestors(mpn);
			// System.out.println("node: " + node_cnt);
			// System.out.println("current-pn: " + current.pn);
			// System.out.println("current-dn: " + current.dn);
		}

		// System.out.println(root.pn);
		// System.out.println(current);
		System.out.println("pns_node: " + node_cnt);

	//	System.out.println("root-pn: " + root.pn);
	//	System.out.println("root-dn: " + root.dn);

		HashMap<Integer, String> path = new HashMap<Integer, String>();
		// pathMap(path);
		// showPath(root, path);

		if (root.pn == 0) {
			//showProof(root);
		}
		if (root.dn == 0) {
			showDisproof(root);
		}
		// System.out.println("node_cnt: " + node_cnt);
		node_cnt = 0;
	}
}