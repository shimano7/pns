package go;

public class ProofNumberSearch2 {
	static final int inf = 10000000;
	static final int or = 1; // OR Node
	static final int and = -1; // AND Node
	static final int proven = 1;
	static final int disproven = -1;
	static final int unknown = 0;
	static final int black = 1;
	static final int white = 2;

	static int[][] black_move_cnt = new int[100][100];// 攻め方が着手した回数
	static int[][] attacker_success = new int[100][100];// 成功した回数
	static double[][] attacker_success_probability = new double[100][100];// 成功率

	static int node_cnt = 0;

	/* Evaluate */
	static void evaluate(GoNode node, int rootColor) {
		type(node, rootColor);
		int eva = GoRule.CaptureStoneRule(node.agehama_cnt);
		if (eva == rootColor) {
			node.evaluate = proven; // root-win
		} else if (eva == GoBoard.ReverseColor(rootColor) || eva == 0)
			node.evaluate = disproven; // root-loss or draw
		else if (eva == -2) {
			node.evaluate = unknown; // unknown
		}
	}

	/* AND Node or OR Node */
	static void type(GoNode node, int rootColor) {
		if (node.color == rootColor)
			node.type = or;
		else if (node.color == GoBoard.ReverseColor(rootColor))
			node.type = and;
		else
			throw new RuntimeException();
	}

	/* Calculating proof and disproof numbers */
	static void setProofNumberAndDisproofNumbers(GoNode node) {
		node_cnt++;
		if (node.pns_expanded) { // Internal node
			if (node.type == and) {
				node.pn = 0;
				node.dn = inf;
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					node.pn = node.pn + child.pn;
					if (child.dn < node.dn) // min
						node.dn = child.dn;
				}
				if (node.pn == 0) {
					node.dn = inf;
					attacker_success[node.move_xy[1]][node.move_xy[0]]++;// 成功した回数を記録
					// 成功率の計算
					attacker_success_probability[node.move_xy[1]][node.move_xy[0]] = (double) attacker_success[node.move_xy[1]][node.move_xy[0]]
							/ (double) black_move_cnt[node.move_xy[1]][node.move_xy[0]];

					System.out
							.print("{"
									+ "("
									+ node.move_xy[1]
									+ "."
									+ node.move_xy[0]
									+ ")"
									+ "成功回数: "
									+ attacker_success[node.move_xy[1]][node.move_xy[0]]
									+ ","
									+ "着手した回数: "
									+ black_move_cnt[node.move_xy[1]][node.move_xy[0]]
									+ ","
									+ "成功率: "
									+ attacker_success_probability[node.move_xy[1]][node.move_xy[0]]);
					System.out.println();

				}

				node.failure = failure_probability(node.move_xy[1],
						node.move_xy[0]);
				System.out.println("pn: " + node.pn);
				System.out.println("move_xy: {" + node.move_xy[1] + ","
						+ node.move_xy[0] + "}");
				System.out.println("failure: " + node.failure + "suc: "
						+ attacker_success[node.move_xy[1]][node.move_xy[0]]);
				for (int j = 1; j < node.size() - 1; j++) {
					for (int i = 1; i < node.size() - 1; i++) {
						System.out.print(node.board[j][i]);
					}
				}
				System.out.println();
				GoBoard.DisplayBoard(node);

				node.pn *= node.failure;
				// System.out.println("pn: " + node.pn);

			} else if (node.type == or) { // OR Node
				node.pn = inf;
				node.dn = 0;
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					node.dn = node.dn + child.dn;
					if (child.pn < node.pn) {// min
						node.pn = child.pn;
					}
				}
				if (node.dn == 0)
					node.pn = inf;

			} else { // Error
				throw new RuntimeException();
			}
		} else { // Terminal or non-Terminal leaf
			switch (node.evaluate) {
			case disproven:
				node.pn = inf;
				node.dn = 0;
				break;
			case proven:
				node.pn = 0;
				node.dn = inf;
				break;
			case unknown:
				node.pn = 1;
				node.dn = 1;
				break;
			}
		}
	}

	/* Select a MPN */
	static GoNode selectMostProvingNode(GoNode node) {
		if (node == null)
			throw new RuntimeException();
		if (node.pn == 0 || node.dn == 0)
			throw new RuntimeException();
		while (node.pns_expanded) {
			double value = inf;
			GoNode best = null;
			if (node.type == or) {
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					if (value > child.pn) {
						best = child;
						value = child.pn;
					}
				}
			} else {
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					if (value > child.dn) {
						best = child;
						value = child.dn;
					}
				}
			}
			if (best == null || best.pn == 0 || best.dn == 0) {
				System.out.println("children.size: " + node.children.size());
				System.out.println("type: " + node.type);
				System.out.println("node.pn: " + node.pn);
				System.out.println("node.dn: " + node.dn);
				if (best != null) {
					System.out.println("best.pn: " + best.pn);
					System.out.println("best.dn: " + best.dn);
				}
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					System.out.println("child.pn: " + child.pn);
					System.out.println("child.dn: " + child.dn);
				}
				throw new RuntimeException();
			}
			node = best;
		}
		return node;
	}

	/* Expand node */
	static void expandNode(GoNode node, int rootColor) {
		for (int y = 1; y < node.size() - 1; y++) {
			for (int x = 1; x < node.size() - 1; x++) {
				if (node.board[y][x] == 0) {
					GoNode child = node.CreateChild(node, x, y, node.color);
					if (node.color == black) {
						black_move_cnt[y][x]++;// 攻め方が着手した場所を記録
					}
					node.children.add(child);
				}
			}
		}
		for (int i = 0; i < node.children.size(); i++) {
			GoNode child = node.children.get(i);
			evaluate(child, rootColor);
			setProofNumberAndDisproofNumbers(child);

		}
		node.pns_expanded = true;
	}

	/* Update ancestors */
	static GoNode updateAncestors(GoNode node) {
		while (true) {
			double old_pn = node.pn;
			double old_dn = node.dn;
			setProofNumberAndDisproofNumbers(node);
			if (node.pn == old_pn && node.dn == old_dn)
				return node;
			if (node.parent == null)
				return node;
			node = node.parent;
		}
	}

	static double failure_probability(int x, int y) {
		double ans = 1 - attacker_success_probability[y][x];
		final double min_prob = 0.0001;
		if (ans == 0)
			return min_prob;
		return ans;
	}

	static void showProof(GoNode node) {
		while (node.children.size() != 0) {
			GoNode child = null;
			if (node.type == or) {
				for (int i = 0; i < node.children.size(); i++) {
					child = node.children.get(i);
					if (child.pn == 0) {
						GoBoard.DisplayBoard(child);
						node = child;
					}
				}
			} else {
				child = node.children.get(0);
				GoBoard.DisplayBoard(child);
				node = child;
			}
		}
	}

	/* PNS */
	static void proofNumberSearch(GoNode root, int rootColor) {
	//	GoRule.RootCoordinates(root);
		root.color = rootColor;
		evaluate(root, rootColor);

		setProofNumberAndDisproofNumbers(root);
		GoNode current = root;

		while (root.pn != 0 && root.dn != 0) {
			GoNode mpn = selectMostProvingNode(current);
			expandNode(mpn, rootColor);
			current = updateAncestors(mpn);
		}

		System.out.println(current);
		System.out.println("root-pn: " + root.pn);
		System.out.println("root-dn: " + root.dn);

		System.out.println();
		System.out.println("攻め方の着手回数: ");
		for (int j = 1; j < root.size() - 1; j++) {
			for (int i = 1; i < root.size() - 1; i++) {
				System.out.print(black_move_cnt[j][i] + ", ");
			}
			System.out.println();
		}
		System.out.println();

		System.out.println("攻めが成功した回数: ");
		for (int j = 1; j < root.size() - 1; j++) {
			for (int i = 1; i < root.size() - 1; i++) {
				System.out.print(+attacker_success[j][i] + ", ");
			}
			System.out.println();
		}
		System.out.println();

		System.out.println("成功率: ");
		for (int j = 1; j < root.size() - 1; j++) {
			for (int i = 1; i < root.size() - 1; i++) {
				System.out.print(+attacker_success_probability[j][i] + ", ");
			}
			System.out.println();
		}
		System.out.println();
		if (root.pn == 0) {
			showProof(root);
		}

		System.out.println("node_cnt2: " + node_cnt);
	}
}