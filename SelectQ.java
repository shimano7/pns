package go;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;

public class SelectQ {
	static final int space = 0;
	static final int black = 1;
	static final int white = 2;
	static final int or = 1; // OR Node
	static final int and = -1; // AND Node

	static HashMap<Integer, ArrayList<Integer>> provenMoves(GoNode p) {
		HashMap<Integer, ArrayList<Integer>> map = new HashMap<Integer, ArrayList<Integer>>();

		ArrayList<Integer> blackList = new ArrayList<Integer>();
		ArrayList<Integer> whiteList = new ArrayList<Integer>();

		Deque<GoNode> queue = new ArrayDeque<GoNode>();
		GoNode node = p;
		queue.push(node);
		while (!queue.isEmpty()) {
			if (node.type == or) {
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					if (child.pn == 0) {
						queue.add(child);
					}
				}
			} else {
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					queue.add(child);
				}
			}
			node = queue.poll();
			if (node.type == or)
				blackList.add(GoRule.XY_Trans(node.move_xy[0], node.move_xy[1]));
			else
				whiteList.add(GoRule.XY_Trans(node.move_xy[0], node.move_xy[1]));
		}
		HashSet<Integer> set = new HashSet<Integer>(blackList);
		ArrayList<Integer> blackList2 = new ArrayList<Integer>(set);

		HashSet<Integer> set2 = new HashSet<Integer>(whiteList);
		ArrayList<Integer> whiteList2 = new ArrayList<Integer>(set2);

		map.put(black, blackList2);
		map.put(white, whiteList2);
		
		return map;
	}

	static boolean similarPosition(GoNode p, GoNode q) {
		HashMap<Integer, ArrayList<Integer>> map = provenMoves(p);
		for (int i = 0; i < map.get(black).size(); i++) {
			if (map.get(black).get(i) != 0) {
				int[] xy = GoRule.RevXY(map.get(black).get(i));
				if (!GoPlay.CheckLegal(q, black, xy[0], xy[1]))
					return false;
			}
		}

		for (int i = 0; i < map.get(white).size(); i++) {
			if (map.get(white).get(i) != 0) {
				int[] xy = GoRule.RevXY(map.get(white).get(i));
				if (!GoPlay.CheckLegal(q, white, xy[0], xy[1]))
					return false;
			}
		}
		return true;
	}

}
