package go;

import java.util.*;

public class Simulation2 {
	static final int inf = 1000000;
	static final int space = 0;
	static final int black = 1;
	static final int or = 1; // OR Node
	static final int and = -1; // AND Node

	static double node_cnt = 0;

	/* p_childの子ノードの中からpn = 0のものを返す */
	static GoNode select(GoNode p_child) {
		GoNode child = null;
		if (p_child.pn == 0) {
			for (int i = 0; i < p_child.children.size(); i++) {
				if (p_child.children.get(i).pn == 0) {
					child = p_child.children.get(i);
				}
			}
		}
		return child;
	}

	static void setProofNumber(GoNode node) {
		WideMore.evaluate(node, black);
		WideMore.setProofNumberAndDisproofNumbers(node);

	}

	static GoNode searchPair(GoNode p, GoNode q, boolean pair_flag) {
		GoNode child = null;
		if (p == null) {
			throw new RuntimeException();
		}
		if (q == null) {
			throw new RuntimeException();
		}
		/* pair_flagがtrueのときはQがORノードの場合 */
		if (pair_flag) {
			for (int i = 0; i < q.children.size(); i++) {
				child = q.children.get(i);
				if (child.st_move.equals(p.st_move)) {
					return child;
				}
			}
		} else {
			for (int i = 0; i < p.children.size(); i++) {
				child = p.children.get(i);
				if (child.pn == 0 && child.dn != 0 && child.st_move.equals(q.st_move)) {
					return child;
				}
			}
		}

		return child;
	}

	static void expandLeaf(GoNode q_child) {
		if (WideMore.pass(q_child)) {
			GoNode child = q_child.CreateChild(q_child, 15, 15, q_child.color);
			q_child.pns_expanded = true;
			WideMore.node_cnt++;
			child.type = q_child.type * (-1);
			q_child.children.add(child);
		}

		/* Qが新規接点のとき */
		if (!GoRule.tumego_black_win(q_child)) {
			for (int y = 1; y < GoNode.Yrange + 1; y++) {
				for (int x = 1; x < GoNode.Xrange + 1; x++) {
					if (GoPlay.CheckLegal(q_child, q_child.color, x, y)) {
						GoNode child = q_child.CreateChild(q_child, x, y, q_child.color);

						WideMore.node_cnt++;
						child.type = q_child.type * (-1);
						q_child.pns_expanded = true;
						setProofNumber(child);
						q_child.children.add(child);
					}
				}
			}
		}
	}

	static ArrayList<GoNode> array_pair(GoNode p, GoNode q) {
		ArrayList<GoNode> list = new ArrayList<GoNode>();
		list.add(p);
		list.add(q);
		return list;
	}


	static void Simulation(GoNode p, GoNode q, int rootColor) {
		if (!q.pns_expanded)
			setProofNumber(q);

		/* PとQをペアでstackに格納する. */
		Deque<ArrayList<GoNode>> stack = new ArrayDeque<ArrayList<GoNode>>();

		stack.push(array_pair(p, q));

		boolean fail = false;

		while (!stack.isEmpty()) {
			ArrayList<GoNode> pair = stack.pop();

			GoNode p_child = pair.get(0);
			GoNode q_child = pair.get(1);

			if (p_child == null) {
				fail = true;
				break;
			}


			if (q_child.type == or) {
				if (p_child.children.size() == 0) {
					fail = true;
					break;
				}
				if (q_child.pn != 0 && !q_child.pns_expanded) {
					expandLeaf(q_child);
					setProofNumber(q_child);
				}

				if (q_child.pn == 0) {
					q_child.success_flag = true;

					GoNode node = q_child.parent;
					while (node != null) {
						WideMore.setProofNumberAndDisproofNumbers(node);
						if (node.pn != 0) {
							break;
						}
						node = node.parent;
					}

					continue;
				}

				if (q_child.dn == 0) {
					GoNode node = q_child.parent;
					while (node != null) {
						WideMore.setProofNumberAndDisproofNumbers(node);
						if (node.dn != 0) {
							break;
						}
						node = node.parent;
					}
					break;
				}

				// P -> P'
				p_child = select(p_child);
				if (p_child == null) {
					fail = true;
					break;
				}
				if (p_child.pn != 0) {
					throw new RuntimeException();
				}
				// Q -> Q'

				q_child = searchPair(p_child, q_child, true);
				if (q_child == null) {
					fail = true;
					break;
				}

				stack.push(array_pair(p_child, q_child));

			} else if (q_child.type == and) {
				if (!q_child.pns_expanded) {
					expandLeaf(q_child);
					setProofNumber(q_child);

				}
				if (q_child.dn == 0) {
					GoNode node = q_child.parent;
					while (node != null) {
						WideMore.setProofNumberAndDisproofNumbers(node);
						if (node.dn != 0) {
							break;
						}
						node = node.parent;
					}
					break;
				}

				if (q_child.pn == 0) {
					q_child.success_flag = true;

					GoNode node = q_child;
					while (node != null) {
						WideMore.setProofNumberAndDisproofNumbers(node);
						if (node.pn != 0) {
							break;
						}
						if (node.type == and) {
							WideMore.attacker_success[node.move_xy[1]][node.move_xy[0]]++;// 成功した回数を記録
						}
						node = node.parent;
					}

					continue;
				}

				for (int i = 0; i < q_child.children.size(); i++) {
					GoNode new_q_child = q_child.children.get(i);
					GoNode new_p_child = searchPair(p_child, new_q_child, false);

					if (p_child != null) {
						if (p_child.pn != 0) {
							System.out.println("p_child.pn != 0 in AND");
							throw new RuntimeException();
						}
						stack.push(array_pair(new_p_child, new_q_child));
					}

				}

			} else {
				System.out.println("or/and Error");
				throw new RuntimeException();
			}
		}
		if (!fail) {
			q.success_flag = true;
		}
	}
}
