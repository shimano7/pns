package go;

public class SimulationTest {
	static final int black = 1;
	static final int white = 2;
	static final int size = 19;

	static void TumegoTestP1(GoNode node) {
		node.board[1][4] = white;
		node.board[2][1] = white;
		node.board[2][2] = white;
		node.board[2][3] = white;
		node.board[2][4] = white;

		node.board[1][5] = black;
		node.board[2][5] = black;
		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][4] = black;
		node.board[3][5] = black;

		node.Xrange = 5;
		node.Yrange = 3;
	}

	static void TumegoTestQ1(GoNode node) {
		node.board[1][4] = white;
		node.board[2][1] = white;
		node.board[2][2] = white;
		node.board[2][3] = white;
		node.board[2][4] = white;

		node.board[1][5] = black;
		node.board[2][5] = black;
		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][4] = black;
		node.board[3][5] = black;
		node.board[4][5] = black;

		node.Xrange = 5;
		node.Yrange = 3;
	}

	static void TumegoTestP2(GoNode node) {
		/* black */
		node.board[1][3] = black;
		node.board[1][8] = black;

		node.board[2][1] = black;
		// node.board[2][4] = black;
		node.board[2][6] = black;
		node.board[2][8] = black;

		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][7] = black;

		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		node.board[4][8] = black;

		node.board[5][3] = black;

		/* white */
		node.board[1][2] = white;
		node.board[1][5] = white;
		node.board[1][6] = white;
		node.board[1][7] = white;

		node.board[2][2] = white;
		node.board[2][3] = white;
		// node.board[2][5] = white;
		node.board[2][7] = white;

		node.board[3][4] = white;
		node.board[3][5] = white;
		node.board[3][6] = white;

		node.Xrange = 7;
		node.Yrange = 3;
	}

	static void TumegoTestQ2(GoNode node) {
		/* black */
		node.board[1][3] = black;
		node.board[1][8] = black;

		// node.board[2][1] = black;
		// node.board[2][4] = black;
		node.board[2][6] = black;
		node.board[2][8] = black;

		node.board[3][1] = black;
		node.board[3][2] = black;
		node.board[3][3] = black;
		node.board[3][7] = black;

		node.board[4][4] = black;
		node.board[4][5] = black;
		node.board[4][6] = black;
		node.board[4][8] = black;

		node.board[5][3] = black;

		/* white */
		node.board[1][2] = white;
		node.board[1][5] = white;
		node.board[1][6] = white;
		node.board[1][7] = white;

		// node.board[2][2] = white;
		node.board[2][3] = white;
		// node.board[2][5] = white;
		node.board[2][7] = white;

		node.board[3][4] = white;
		node.board[3][5] = white;
		node.board[3][6] = white;

		node.Xrange = 7;
		node.Yrange = 3;
	}

	/* p25 */
	static void TumegoTestP3(GoNode node) {
		// black
		int[] setBlack = { 21, 31, 42, 62, 53, 54, 55, 45, 35, 36, 37, 27, 17, 41, 51, 46, 65, 47, 52, 55 };
		// white
		int[] setWhite = { 12, 14, 22, 25, 26, 32, 34, 43, 44 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;

	}

	static void TumegoTestQ3(GoNode node) {

		// black
		int[] setBlack = { 21, 31, 42, 62, 53, 54, 55, 45, 35, 36, 37, 27, 17, 41, 51, 46, 65, 47, 52 };
		// white
		int[] setWhite = { 14, 22, 25, 26, 32, 34, 43, 44 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	/* p26 */
	static void TumegoTestP4(GoNode node) {
		// black
		int[] setBlack = { 21, 31, 23, 41, 42, 43, 44, 45, 36, 37, 28, 18, 46, 47, 48, 38 };
		// white
		int[] setWhite = { 12, 22, 32, 33, 35, 24, 26, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 3;

	}

	static void TumegoTestQ4(GoNode node) {

		// black
		int[] setBlack = { 21, 31, 23, 41, 42, 43, 44, 45, 36, 37, 28, 18, 46, 47, 48, 38 };
		// white
		int[] setWhite = { 12, 22, 33, 35, 24, 26, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 3;
	}

	/* p34 */
	static void TumegoTestP5(GoNode node) {
		// black
		int[] setBlack = { 41, 42, 61, 62, 63, 53, 54, 55, 56, 57, 45, 46, 47, 17, 27, 37, 26, 36, 35, 33, 25 };
		// white
		int[] setWhite = { 12, 23, 24, 43, 44, 32, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	static void TumegoTestQ5(GoNode node) {
		// black
		int[] setBlack = { 41, 42, 61, 62, 63, 53, 54, 55, 56, 57, 45, 46, 47, 17, 27, 37, 26, 36, 35, 33, 25 };
		// white
		int[] setWhite = { 12, 23, 24, 43, 44, 32 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	static void TumegoTestP6(GoNode node) {
		// black
		int[] setBlack = { 31, 32, 42, 43, 44, 45, 27, 38, 57, 47, 56, 17, 41, 46, 37 };
		// white
		int[] setWhite = { 26, 36, 34, 33, 22, 21, 11 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;
	}

	static void TumegoTestSameP6(GoNode node) {
		// black
		int[] setBlack = { 31, 32, 42, 43, 44, 45, 27, 38, 57, 47, 56, 17, 41, 46, 37 };
		// white
		int[] setWhite = { 26, 36, 34, 33, 22, 21, 11 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;
	}

	static void TumegoTestQ6(GoNode node) {
		// black
		int[] setBlack = { 31, 32, 42, 43, 44, 45, 27, 38, 57, 47, 56, 17, 41, 46, 37 };
		// white
		int[] setWhite = { 26, 36, 34, 33, 22, 21 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;
	}

	static void TumegoTestP7(GoNode node) {
		// black
		int[] setBlack = { 17, 27, 35, 36, 37, 41, 42, 43, 44, 45, 46, 31 };
		// white
		int[] setWhite = { 13, 14, 22, 25, 26, 33, 34, };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;
	}

	static void TumegoTestQ7(GoNode node) {
		// black
		int[] setBlack = { 17, 27, 35, 36, 37, 41, 42, 43, 44, 45, 46 };
		// white
		int[] setWhite = { 13, 14, 22, 25, 26, 33, 34 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;
	}

	static void TumegoTestP8(GoNode node) {
		int[] setBlack = { 17, 27, 35, 36, 37, 41, 42, 43, 44, 45, 46 };
		// white
		int[] setWhite = { 12, 25, 26, 32, 33, 34 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;
	}

	static void TumegoTestQ8(GoNode node) {
		int[] setBlack = { 17, 28, 38, 35, 36, 37, 41, 42, 43, 44, 45, 46, 18 };
		// white
		int[] setWhite = { 12, 25, 26, 32, 33, 34, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 3;
	}

	/* p33 */
	static void TumegoTestP9(GoNode node) {
		int[] setBlack = { 21, 31, 32, 43, 44, 45, 46, 47, 48, 38, 28, 18, 17 };
		// white
		int[] setWhite = { 12, 22, 33, 34, 35, 36, 37, 27, 16, };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 3;
	}

	static void TumegoTestQ9(GoNode node) {
		int[] setBlack = { 21, 31, 32, 43, 44, 45, 46, 47, 48, 38, 17, 29, 39, 49, 18, 19 };
		// white
		int[] setWhite = { 12, 22, 33, 34, 35, 36, 37, 27, 16, 28 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 8;
		node.Yrange = 3;
	}

	/* p55 */
	static void TumegoTestP10(GoNode node) {
		int[] setBlack = { 21, 31, 32, 43, 44, 45, 46, 47, 48, 38, 28, 18, 41, 42, 12 };
		// white
		int[] setWhite = { 17, 27, 37, 36, 25, 33, 23, 22 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 3;
	}

	static void TumegoTestQ10(GoNode node) {
		int[] setBlack = { 12, 21, 31, 32, 43, 44, 45, 46, 47, 48, 38, 29, 19, 39, 18, 41, 42 };
		// white
		int[] setWhite = { 17, 27, 37, 36, 25, 33, 23, 22, 28 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 8;
		node.Yrange = 3;
	}

	/* p171 */
	static void TumegoTestP11(GoNode node) {
		int[] setBlack = { 31, 41, 42, 52, 53, 54, 55, 46, 36, 26, 16, 12, 45, 51 };
		// white
		int[] setWhite = { 21, 22, 32, 14, 34, 44, 25, 35 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 5;
		node.Yrange = 4;
	}

	static void TumegoTestQ11(GoNode node) {
		int[] setBlack = { 31, 41, 42, 52, 53, 54, 55, 46, 36, 27, 16, 12, 45, 51, 17, 27, 37, 47 };
		// white
		int[] setWhite = { 21, 22, 32, 34, 44, 25, 35, 26 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	/* p183 */
	static void TumegoTestP12(GoNode node) {
		int[] setBlack = { 31, 32, 33, 43, 44, 45, 46, 47, 37, 27, 17, 21, 12, 24, 42 };
		// white
		int[] setWhite = { 11, 21, 22, 23, 33, 34, 35, 36, 26 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	static void TumegoTestQ12(GoNode node) {
		int[] setBlack = { 31, 32, 33, 43, 44, 45, 46, 47, 37, 27, 17, 12, 24, 42 };
		// white
		int[] setWhite = { 22, 23, 33, 34, 35, 36, 26 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	/* p77 */
	static void TumegoTestP13(GoNode node) {
		int[] setBlack = { 21, 31, 32, 33, 43, 44, 45, 46, 47, 48, 38, 28, 18 };
		// white
		int[] setWhite = { 22, 23, 14, 34, 35, 36, 37, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 3;
	}

	static void TumegoTestQ13(GoNode node) {
		int[] setBlack = { 21, 31, 32, 33, 43, 44, 45, 46, 47, 48, 38, 28, 18 };
		// white
		int[] setWhite = { 23, 14, 34, 35, 36, 37, 27 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 3;
	}

	/* p83 */
	static void TumegoTestP14(GoNode node) {
		int[] setBlack = { 41, 51, 52, 53, 43, 33, 54, 55, 56, 46, 36, 27, 17, 37, 12 };
		// white
		int[] setWhite = { 21, 31, 32, 42, 13, 23, 25, 35, 45, 16 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	static void TumegoTestQ14(GoNode node) {
		int[] setBlack = { 41, 52, 53, 43, 33, 54, 55, 56, 46, 36, 27, 17, 37, 12 };
		// white
		int[] setWhite = { 21, 32, 42, 13, 23, 25, 35, 45, 16 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	/* p57 */
	static void TumegoTestP15(GoNode node) {
		int[] setBlack = { 41, 51, 52, 53, 54, 55, 56, 46, 36, 26, 16, 15, 24 };
		// white
		int[] setWhite = { 31, 22, 42, 43, 34, 14, 35, 25 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 5;
		node.Yrange = 4;
	}

	static void TumegoTestQ15(GoNode node) {
		int[] setBlack = { 41, 51, 52, 53, 54, 55, 56, 46, 36, 26, 16, 15, 24 };
		// white
		int[] setWhite = { 31, 22, 42, 43, 14, 35, 25 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 5;
		node.Yrange = 4;

	}

	/* p59 */
	static void TumegoTestP16(GoNode node) {
		int[] setBlack = { 11, 21, 31, 32, 42, 52, 53, 54, 55, 56, 46, 47, 37, 27, 17, 41, 51, 35 };
		// white
		int[] setWhite = { 22, 13, 33, 44, 45, 36, 26, 25 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	static void TumegoTestQ16(GoNode node) {
		int[] setBlack = { 11, 21, 31, 32, 42, 52, 53, 54, 55, 56, 46, 47, 37, 27, 17, 41, 51, 35 };
		// white
		int[] setWhite = { 22, 13, 33, 45, 36, 26, 25 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	static void TumegoTestP17(GoNode node) {
		int[] setBlack = { 21, 31, 41, 42, 43, 44, 45, 46, 47, 48, 38, 39, 29, 19, 35, 26, 32 };
		// white
		int[] setWhite = { 33, 34, 23, 25, 36, 37, 27, 17, 28 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 8;
		node.Yrange = 3;
	}

	static void TumegoTestQ17(GoNode node) {
		int[] setBlack = { 21, 31, 41, 42, 43, 44, 45, 46, 47, 48, 38, 39, 29, 19, 35, 26 };
		// white
		int[] setWhite = { 32, 33, 34, 23, 25, 36, 37, 27, 17, 28 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 8;
		node.Yrange = 3;
	}

	static void TumegoTestP18(GoNode node) {
		int[] setBlack = { 11, 21, 31, 41, 32, 42, 43, 44, 54, 55, 56, 57, 47, 37, 27, 17 };
		// white
		int[] setWhite = { 12, 22, 23, 34, 45, 15, 26, 36 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	static void TumegoTestQ18(GoNode node) {
		int[] setBlack = { 11, 21, 31, 41, 32, 42, 43, 44, 54, 55, 56, 57, 47, 37, 27, 17 };
		// white
		int[] setWhite = { 23, 34, 45, 15, 26, 36 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	/* p91 */
	static void TumegoTestP19(GoNode node) {
		int[] setBlack = { 41, 42, 32, 43, 44, 54, 55, 56, 57, 58, 48, 38, 28, 18, 17, 27, 16, 37 };
		// white
		int[] setWhite = { 21, 31, 22, 33, 34, 45, 26, 15, 36 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 4;
	}

	static void TumegoTestQ19(GoNode node) {
		int[] setBlack = { 41, 42, 32, 43, 44, 54, 55, 56, 57, 58, 48, 38, 28, 18, 17, 27, 16 };
		// white
		int[] setWhite = { 21, 31, 22, 33, 34, 45, 36, 26, 37, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 4;
	}

	/* 107 */
	static void TumegoTestP20(GoNode node) {
		int[] setBlack = { 41, 42, 52, 53, 54, 55, 45, 46, 36, 37, 27, 17, 16, 15, 33 };
		// white
		int[] setWhite = { 22, 32, 43, 44, 34, 14, 25, 35 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 4;
	}

	static void TumegoTestQ20(GoNode node) {
		int[] setBlack = { 41, 42, 52, 53, 54, 55, 45, 46, 36, 37, 28, 17, 16, 15, 33, 47 };
		// white
		int[] setWhite = { 22, 32, 43, 44, 34, 14, 25, 35 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 7;
		node.Yrange = 4;
	}

	static void TumegoTestP21(GoNode node) {
		int[] setBlack = { 31, 41, 42, 43, 44, 45, 46, 36, 26, 27, 17, 14 };
		// white
		int[] setWhite = { 21, 22, 32, 34, 35, 25, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;
	}

	static void TumegoTestQ21(GoNode node) {
		int[] setBlack = { 31, 41, 42, 43, 44, 45, 46, 36, 27, 17, 14 };
		// white
		int[] setWhite = { 21, 22, 32, 34, 35, 25, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;
	}

	static void TumegoTestP22(GoNode node) {
		// black
		int[] setBlack = { 12, 14, 23, 17, 26, 27, 36, 46, 41, 42, 43, 44, 45, 31 };
		// white
		int[] setWhite = { 21, 22, 32, 33, 34, 35, 25, 15, 16 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;

	}

	static void TumegoTestQ22(GoNode node) {
		// black
		int[] setBlack = { 14, 17, 26, 27, 36, 46, 41, 42, 43, 44, 45, 31 };
		// white
		int[] setWhite = { 21, 22, 32, 34, 35, 25, 15 };

		MakePosition.makePosition(node, setBlack, setWhite);
		node.Xrange = 6;
		node.Yrange = 3;

	}

	public static void main(String[] args) {
		int[][] board = new int[size + 2][size + 2];
		GoNode p = new GoNode(board);
		GoBoard.InitializeBoard(p);

		// TumegoTestP2(p);

		GoNode q = new GoNode(board);
		GoBoard.InitializeBoard(q);

		// TumegoTestQ2(q);

		// TumegoTestP1(p);
		// TumegoTestQ1(q);

		TumegoTestP1(p);
		// TumegoTestSameP6(q);

		TumegoTestQ1(q);

		if (p != null) {
			System.out.println("P");
			GoBoard.DisplayBoard(p);
		}
		if (q != null) {
			System.out.println("Q");
			GoBoard.DisplayBoard(q);
		}
		// System.out.println(AddSuccess.degreeOfSimilarity(p, q));
		ProofNumberSearch.proofNumberSearch(p, black);
		// System.out.println(SimilarPosition.similarPosition(p,q));
		q.type = 1;
		q.color = black;
		q.pn = 1;
		q.dn = 1;
		Simulation2.Simulation(p, q, black);

		// double rate = Mismatch.mismatchRate(p, q);
		// System.out.println(rate);
		// ProofNumberSearch.proofNumberSearch(q, black);
		// KishimotoSimulation.proofNumberSearch(q, black);
		// System.out.println(GoRule.black_win(p));

	}
}
