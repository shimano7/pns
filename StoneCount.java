package go;

public class StoneCount {

	static int stoneCount(GoNode node, int color) {
		int count = 0;
		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if(node.board[y][x] == color){
					count++;
				}
			}
		}
		return count;
	}
}
