package go;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class WideMore {
	static final int inf = 10000000;
	static final int or = 1; // OR Node
	static final int and = -1; // AND Node
	static final int proven = 1;
	static final int disproven = -1;
	static final int unknown = 0;
	static final int space = 0;
	static final int black = 1;
	static final int white = 2;
	static boolean koFlag = false;
	static int koMove = 0;

	static int node_cnt = 0;
	static double sim_run_cnt = 0;
	static double sim_success_cnt = 0;

	static int[][] black_move_cnt = new int[100][100];// 攻め方が着手した回数
	static int[][] attacker_success = new int[100][100];// 成功した回数
	static double[][] attacker_success_probability = new double[100][100];// 成功率

	static double R = 0.9;
	static int p_visit_limit = 10;
	static int q_visit_limit = 1;
	static int q_depth_limit = 4;

	static boolean deepPN_run = false;
	static boolean pns_run = true;

	static int root_white = 0;

	static HashMap<Integer, Double> pn_map = new HashMap<Integer, Double>();
	static HashMap<Integer, Double> dn_map = new HashMap<Integer, Double>();

	static boolean end_search(GoNode node) {
		int legal_cnt = 0;
		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if (GoPlay.CheckLegal(node, node.color, x, y))
					legal_cnt++;
			}
		}
		if (legal_cnt == 0) {
			return true;
		}
		return false;
	}

	static boolean pass(GoNode node) {
		if (node.color == white) {
			if (end_search(node))
				return true;

			int space_cnt = 0;
			int black_legal_cnt = 0;
			for (int y = 1; y < GoNode.Yrange + 1; y++) {
				for (int x = 1; x < GoNode.Xrange + 1; x++) {
					if (node.board[y][x] == space)
						space_cnt++;
					if (GoPlay.CheckLegal(node, black, x, y))
						black_legal_cnt++;
				}
			}
			if (space_cnt == 2 && black_legal_cnt == 0)
				return true;
		}
		return false;
	}

	/* Evaluate */
	static void evaluate(GoNode node, int rootColor) {
		type(node, rootColor);
		if (GoRule.tumego_black_win(node)) {
			node.evaluate = proven; // root-win
		} else if (end_search(node) && node.color == black)
			node.evaluate = disproven; // root-loss or draw
		else {
			node.evaluate = unknown; // unknown
		}
	}

	/* AND Node or OR Node */
	static void type(GoNode node, int rootColor) {
		if (node.color == rootColor)
			node.type = or;
		else if (node.color == GoBoard.ReverseColor(rootColor))
			node.type = and;
		else
			throw new RuntimeException();
	}

	/* DPNの設定 */
	static void setDPN(GoNode node) {
		if (deepPN_run) {
			if (node.type == and) {
				if (node.pn == 0 || node.dn == 0) {
					node.dpn = inf;
				} else {
					node.dpn = (1 - 1.0 / node.pn) * R + node.deep * (1.0 - R);
				}
			} else if (node.type == or) {
				if (node.dn == 0 || node.pn == 0) {
					node.dpn = inf;
				} else {
					node.dpn = (1 - 1.0 / node.dn) * R + node.deep * (1.0 - R);
				}
			}
		}
	}

	/* Calculating proof and disproof numbers */
	static void setProofNumberAndDisproofNumbers(GoNode node) {
		node_cnt++;
		boolean weak = false;
		boolean rand_pns = true;
		if (node.pns_expanded) { // Internal node
			int k = 0;

			if (node.type == and) { // AND node
				node.pn = 0;
				node.dn = inf;

				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					if (weak) {
						k++;
						if (child.pn > node.pn) // max
							node.pn = child.pn;
					}
					if (!weak) {
						node.pn = node.pn + child.pn;
						if (node.pn > inf) {
							node.pn = inf;
						}
					}
					if (child.dn < node.dn) // min
						node.dn = child.dn;
				}
				if (weak) {
					node.pn = node.pn + (k - 1);
				}
				Heuristic.success_probability(node, false);
			} else { // OR Node
				node.pn = inf;
				node.dn = 0;
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					if (weak) {
						k++;
						if (child.dn > node.dn) // max
							node.dn = child.dn;
					}
					if (!weak) {
						node.dn = node.dn + child.dn;
						if (node.dn > inf) {
							node.dn = inf;
						}
					}
					if (child.pn < node.pn) // min
						node.pn = child.pn;
				}
				if (weak) {
					node.dn = node.dn + (k - 1);
				}

			}
			if (deepPN_run) {
				GoNode nc = null;
				double val = inf;
				for (int i = 0; i < node.children.size(); i++) {
					GoNode child = node.children.get(i);
					if (child.pn != 0 && child.dn != 0) {
						if (child.dpn < val) {
							nc = child;
							val = nc.dpn;
						}
					}
				}
				if (nc != null) {
					node.deep = nc.deep;
				} else {
					node.deep = -1;
				}
				setDPN(node);
			}
		} else

		{ // Terminal or non-Terminal leaf
			node.deep = 1 / (node.depth + 1.0);
			// node.deep = StoneCount.stoneCount(node,
			// white)/(StoneCount.stoneCount(node,
			// black)+StoneCount.stoneCount(node, white));
			// node.deep = StoneCount.stoneCount(node, white) / root_white;
			switch (node.evaluate) {
			case disproven:
				node.pn = inf;
				node.dn = 0;

				break;
			case proven:
				node.pn = 0;
				node.dn = inf;
				break;
			case unknown:
				node.pn = 1;
				node.dn = 1;
				if (rand_pns) {
					if (pn_map.containsKey(node.zob) && dn_map.containsKey(node.zob)) {
						node.pn = pn_map.get(node.zob);
						node.dn = dn_map.get(node.zob);
					} else {
						double p = Math.random();
						if (p < 0.1) {
							node.pn = node.pn + 1;
						} else if (p < 0.2) {
							node.dn = node.dn + 1;
						}
						pn_map.put(node.zob, node.pn);
						dn_map.put(node.zob, node.dn);
					}
				}
				break;
			}
			setDPN(node);

		}
	}

	/* Select a MPN */
	static GoNode selectMostProvingNode(GoNode node) {
		if (node == null)
			throw new RuntimeException();
		if (node.pn == 0) {
			throw new RuntimeException("pn = 0");

		}
		int depth = 0;
		while (true) {
			if (node == null) {
				throw new RuntimeException();
			}

			if (!node.pns_expanded)
				break;

			double value = inf;
			GoNode best = null;

			for (int i = 0; i < node.children.size(); i++) {
				GoNode child = node.children.get(i);

				if (child.ko[0] != 0 && child.ko[1] != 0)
					child.ko[2] = depth;
				if (node.parent != null && node.parent.parent != null && child.ko[0] == node.parent.parent.move_xy[0]
						&& child.ko[1] == node.parent.parent.move_xy[1] && child.ko[2] > 0 && child.ko[0] > 0
						&& child.ko[1] > 0 && node.parent.parent.move_xy[0] > 0 && node.parent.parent.move_xy[1] > 0)
					koFlag = true;
				else
					koFlag = false;

				if (koFlag && child.color == black) {
					child.pn = inf;
					child.dn = 0;
					child.dpn = inf - 1;

					node.pn = inf;
					node.dn = 0;
					node.dpn = inf - 1;

					return node;
				}

				if (deepPN_run) {
					if (value > child.dpn) {
						best = child;
						value = child.dpn;
					}
				}

				if (pns_run) {
					if (node.type == or) {
						if (value > child.pn) {
							best = child;
							value = child.pn;
						}
					} else {
						if (value > child.dn) {
							best = child;
							value = child.dn;
						}
					}
				}
			}

			++depth;
			node = best;

			node.visit_cnt++;
		}

		return node;
	}

	/* Expand node */
	static void expandNode(GoNode node, int rootColor) {
		if (pass(node)) {
			GoNode child = node.CreateChild(node, 15, 15, node.color);
			child.zob = node.zob;
			node.children.add(child);
		}

		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if (GoPlay.CheckLegal(node, node.color, x, y)) {
					GoNode child = node.CreateChild(node, x, y, node.color);
					if (node.agehama_cnt[0] >= child.agehama_cnt[0] || node.agehama_cnt[1] >= child.agehama_cnt[1]) {
						if (node.color == black) {
							child.zob = node.zob ^ Zobristhash.blackHash.get(GoRule.XY_Trans(x, y));
						} else if (node.color == white) {
							child.zob = node.zob ^ Zobristhash.whiteHash.get(GoRule.XY_Trans(x, y));
						}
					} else {
						nodeHash(child);
					}
					if (node.color == black) {
						black_move_cnt[y][x]++;// 攻め方が着手した場所を記録
					}
					node.children.add(child);

				}
			}
		}

		for (int i = 0; i < node.children.size(); i++) {
			GoNode child = node.children.get(i);
			evaluate(child, rootColor);
			setProofNumberAndDisproofNumbers(child);

		}
		node.pns_expanded = true;
	}

	/* Update ancestors */
	static GoNode updateAncestors(GoNode node, Deque<GoNode> proof_que) {
		while (true) {
			double old_pn = node.pn;
			setProofNumberAndDisproofNumbers(node);

			if (old_pn != 0 && node.pn == 0 && node.visit_cnt >= p_visit_limit) {
				proof_que.offer(node);
			}
			if (node.parent == null) {
				return node;
			}
			node = node.parent;
		}
	}

	static ArrayList<GoNode> searchSiblingPlus(GoNode p, boolean kishimoto) {
		ArrayList<GoNode> q_list = new ArrayList<GoNode>();
		if (p.parent != null) {
			for (int i = 0; i < p.parent.children.size(); i++) {
				GoNode child = p.parent.children.get(i);
				if (child.visit_cnt >= q_visit_limit && child != p && child.pn != 0 && child.dn != 0) {
					q_list.add(child);
				}
			}
		}
		if (!kishimoto) {
			if (p.parent == null || p.parent.parent == null) {
				return q_list;
			}
			GoNode root = p.parent.parent;
			Deque<GoNode> queue = new ArrayDeque<GoNode>();
			queue.offer(root);

			while (!queue.isEmpty()) {
				GoNode child = queue.poll();
				if (p.parent != null && child.parent != null && child.id != p.id && child.parent.id != p.parent.id
						&& child.visit_cnt >= q_visit_limit && child.pn != 0 && child.dn != 0 && child.type == or
						&& SelectQ.similarPosition(p, child)) {
					q_list.add(child);
				}
				for (int i = 0; i < child.children.size(); i++) {
					if (child.depth - root.depth <= q_depth_limit && child.visit_cnt > 0 && child.pn != 0
							&& child.dn != 0) {
						queue.offer(child.children.get(i));
					}
				}
			}
		}
		return q_list;
	}

	static void nodeHash(GoNode node) {
		int xor = 0;
		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				if (node.board[y][x] == black) {
					xor = xor ^ Zobristhash.blackHash.get(GoRule.XY_Trans(x, y));
					// System.out.println(xor);
				} else if (node.board[y][x] == white) {
					xor = xor ^ Zobristhash.whiteHash.get(GoRule.XY_Trans(x, y));
				}
			}
		}
		node.zob = xor;
	}

	/* PNS */
	static void proofNumberSearch(GoNode root, int rootColor, boolean simulation, boolean kishimoto)
			throws IOException {

		node_cnt = 0;
		Zobristhash.zobrist();
		nodeHash(root);
		int search_node_limit = 5000000;
		root_white = StoneCount.stoneCount(root, white);
		GoRule.RootCoordinates(root);
		root.color = rootColor;

		evaluate(root, rootColor);
		setProofNumberAndDisproofNumbers(root);
		Deque<GoNode> proof_que = new ArrayDeque<GoNode>();

		GoNode current = root;

		while (root.pn != 0 && root.dn != 0) {
			if (search_node_limit < node_cnt) {
				break;
			}
			GoNode mpn = null;
			mpn = selectMostProvingNode(current);
			if (mpn.pn != 0 && mpn.dn != 0) {
				expandNode(mpn, rootColor);
			}
			current = updateAncestors(mpn, proof_que);

			// System.out.println("mpn: "+mpn.pn);

			if (simulation) {
				if (!proof_que.isEmpty()) {
					GoNode p = proof_que.poll();// searchProvenNode(current);
					if (p != null) {

						ArrayList<GoNode> q_list = new ArrayList<GoNode>();
						q_list = searchSiblingPlus(p, kishimoto);

						if (!q_list.isEmpty()) {
							for (int i = 0; i < q_list.size(); i++) {
								GoNode q = q_list.get(i);
								Simulation2.Simulation(p, q, black);
								sim_run_cnt += 1;

								if (q.success_flag) {
									sim_success_cnt += 1;
								}
							}
						}
					}
				}
			}
		}
	}
}