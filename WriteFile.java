package go;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.PrintWriter;
import java.io.IOException;

public class WriteFile {

	public static void main(String args[]) {
		String[] s1 = read1();
		String[] s2 = read2();
		String[] s3 = read3();
		String[] s4 = read4();

		try {
			File file = new File("/Users/shimanotakuya/Desktop/kishimoto_addmoves_suc.data");

			// System.out.println(checkBeforeWritefile(file));
			if (checkBeforeWritefile(file)) {
				PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter(file)));

				for (int i = 0; i < s1.length; i++) {
					pw.print(s1[i]+" "+s2[i]+" "+s3[i]+" "+s4[i]);
					pw.println();
				}

				pw.close();
			} else {
				System.out.println("ファイルに書き込めません");
			}
		} catch (IOException e) {
			System.out.println(e);
		}
	}

	private static boolean checkBeforeWritefile(File file) {

		if (file.exists()) {
			if (file.isFile() && file.canWrite()) {
				return true;
			}
		}

		return false;
	}

	public static String[] read1() {
		String[] s = new String[41];
		try {
			File file = new File("/Users/shimanotakuya/Desktop/kishimoto_suc_node.data");

			if (checkBeforeReadfile(file)) {
				BufferedReader br = new BufferedReader(new FileReader(file));

				int k = 0;
				String str;
				while ((str = br.readLine()) != null) {
					// System.out.println(str);
					s[k] = str;
					k++;
				}

				br.close();
			} else {
				System.out.println("ファイルが見つからないか開けません");
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		return s;
	}

	public static String[] read2() {
		String[] s = new String[41];
		try {
			File file = new File("/Users/shimanotakuya/Desktop/addmoves_suc_node.data");

			if (checkBeforeReadfile(file)) {
				BufferedReader br = new BufferedReader(new FileReader(file));

				int k = 0;
				String str;
				while ((str = br.readLine()) != null) {
					// System.out.println(str);
					s[k] = str;
					k++;
				}

				br.close();
			} else {
				System.out.println("ファイルが見つからないか開けません");
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		return s;
	}
	
	public static String[] read3() {
		String[] s = new String[41];
		try {
			File file = new File("/Users/shimanotakuya/Desktop/kishimoto_suc_time.data");

			if (checkBeforeReadfile(file)) {
				BufferedReader br = new BufferedReader(new FileReader(file));

				int k = 0;
				String str;
				while ((str = br.readLine()) != null) {
					// System.out.println(str);
					s[k] = str;
					k++;
				}

				br.close();
			} else {
				System.out.println("ファイルが見つからないか開けません");
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		return s;
	}
	
	public static String[] read4() {
		String[] s = new String[41];
		try {
			File file = new File("/Users/shimanotakuya/Desktop/addmoves_suc_time.data");

			if (checkBeforeReadfile(file)) {
				BufferedReader br = new BufferedReader(new FileReader(file));

				int k = 0;
				String str;
				while ((str = br.readLine()) != null) {
					// System.out.println(str);
					s[k] = str;
					k++;
				}

				br.close();
			} else {
				System.out.println("ファイルが見つからないか開けません");
			}
		} catch (FileNotFoundException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		return s;
	}
	
	private static boolean checkBeforeReadfile(File file) {
		if (file.exists()) {
			if (file.isFile() && file.canRead()) {
				return true;
			}
		}

		return false;
	}
}
