package go;

import java.util.HashMap;
import java.util.Random;

public class Zobristhash {

	static HashMap<Integer, Integer> blackHash = new HashMap<Integer, Integer>();
	static HashMap<Integer, Integer> whiteHash = new HashMap<Integer, Integer>();

	static int rand_size = (int) Math.pow(2, 16);
	static double[] pnRand = new double[rand_size];
	static double[] dnRand = new double[rand_size];

	static void zobrist() {
		Random rand = new Random();
		for (int y = 1; y < GoNode.Yrange + 1; y++) {
			for (int x = 1; x < GoNode.Xrange + 1; x++) {
				blackHash.put(GoRule.XY_Trans(x, y), rand.nextInt(Integer.MAX_VALUE));
				whiteHash.put(GoRule.XY_Trans(x, y), rand.nextInt(Integer.MAX_VALUE));
			}
		}

		for (int i = 0; i < rand_size; i++) {
			pnRand[i] = Math.random() / 10;
			dnRand[i] = Math.random() / 10;
		}
	}
}
